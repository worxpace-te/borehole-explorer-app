﻿using System;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Widget;

using Java.IO;

using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;

namespace BoreholeExplorer
{
	[Activity (Label = "SiteImageCaptureActivity")]			
	public class SiteImageCaptureActivity : Activity
	{
		private File m_CameraImageDirectory;
		private File m_CapturedImageFile;
		private SiteImageView m_imageView;

		private int m_JobIndex;
		private int m_HoleIndex;

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);

			if (m_CapturedImageFile == null) {
				System.Console.WriteLine ("Error - Activity Result returned null Caputured Image File");
				return;
			}

			var localCapturedImageFile = new File (JobDataController.Instance.GetSiteImagesDirectory(), m_CapturedImageFile.Name );

			if ( m_CapturedImageFile.Exists() ) {
				// We successfully captured the image
				// check if our local copy exists - if it does, we delete it here so we can copy the new one over
				if (localCapturedImageFile.Exists ()) {
					localCapturedImageFile.Delete ();
				}
				System.IO.File.Copy( m_CapturedImageFile.Path, System.IO.Path.Combine( JobDataController.Instance.GetSiteImagesDirectory(), m_CapturedImageFile.Name ) );
			}

			// make it available in the gallery
			Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
			Uri contentUri = Uri.FromFile(localCapturedImageFile);
			mediaScanIntent.SetData(contentUri);
			SendBroadcast(mediaScanIntent);

			// display in ImageView. We will resize the bitmap to fit the display
			// Loading the full sized image will consume to much memory 
			// and cause the application to crash.
			using (Bitmap bitmap = localCapturedImageFile.Path.LoadAndResizeBitmap(m_imageView.m_TargetWidth, m_imageView.m_TargetHeight))
			{
				m_imageView.SetImageBitmap(bitmap);
			}
		}

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.SiteImageCaptureView);

			m_JobIndex = Intent.GetIntExtra ("JobIndex", 0);
			m_HoleIndex = Intent.GetIntExtra ("HoleIndex", 0);

			if (IsThereAnAppToTakePictures())
			{
				CreateDirectoryForPictures();

				Button button = FindViewById<Button>(Resource.Id.sicCaptureImageButton);
				m_imageView = FindViewById<SiteImageView>(Resource.Id.sicImageView);

				button.Click += TakeAPicture;
			}

			m_imageView.OnSizeChangedEvent += JobSizeImageViewChangedSizeHandler;
		}

		private void JobSizeImageViewChangedSizeHandler() {
			System.Console.WriteLine ("JobSizeImageViewChangedSizeHandler");
			LoadJobSiteBitmap ();
		}

		private void LoadJobSiteBitmap() {
			System.Console.WriteLine ("Load Job Site Bitmap");

			var jobSiteImageFile = new Java.IO.File (JobDataController.Instance.GetSiteImagesDirectory (), JobDataController.Instance.GetSiteImageFilename (m_JobIndex, m_HoleIndex));

			if (jobSiteImageFile.Exists ()) {
				// we have a file - lets draw it
				System.Console.WriteLine ("Site Image Capture Image View w:" + m_imageView.m_TargetWidth + " h:" + m_imageView.m_TargetHeight);
				using (Bitmap bitmap = jobSiteImageFile.Path.LoadAndResizeBitmap (m_imageView.m_TargetWidth, m_imageView.m_TargetHeight)) {
					System.Console.WriteLine ("Image Loaded - yay!");
					m_imageView.m_CurrentBitMap = jobSiteImageFile.Name;
					m_imageView.SetImageBitmap (bitmap);
				}
			}
		}

		protected override void OnResume ()
		{
			base.OnResume ();

			LoadJobSiteBitmap ();
		}

		private void CreateDirectoryForPictures()
		{
			m_CameraImageDirectory = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), "BoreholeExplorerSiteImages");
			if (!m_CameraImageDirectory.Exists())
			{
				m_CameraImageDirectory.Mkdirs();
			}
		}

		private bool IsThereAnAppToTakePictures()
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);
			IList<ResolveInfo> availableActivities = PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
			return availableActivities != null && availableActivities.Count > 0;
		}

		private void TakeAPicture(object sender, EventArgs eventArgs)
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);

			m_CapturedImageFile = new File (m_CameraImageDirectory, JobDataController.Instance.GetSiteImageFilename( m_JobIndex, m_HoleIndex));

			if (m_CapturedImageFile.Exists()) {
				m_CapturedImageFile.Delete ();
			}

			intent.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(m_CapturedImageFile));

			StartActivityForResult(intent, 0);
		}
	}
}
