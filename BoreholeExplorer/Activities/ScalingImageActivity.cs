﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using UrlImageViewHelper;
using Android.Content.PM;

namespace BoreholeExplorer
{
	[Activity (Label = "ScalingImageActivity", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class ScalingImageActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.ScalingImageView);

			var linearLayout = FindViewById<LinearLayout> (Resource.Id.scalingImageLinearLayout);
			var imageViewExt = new ImageViewExt (this);
			LinearLayout.LayoutParams param = new LinearLayout.LayoutParams (LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.MatchParent);
			imageViewExt.LayoutParameters = param;
			imageViewExt.SetScaleType (ImageView.ScaleType.FitCenter);
			//imageViewExt.SetImageResource (Resource.Drawable.joblist_ok);
			//imageViewExt.SetUrlDrawable (Intent.GetStringExtra ("ImageURL"));
			var imageURL = Intent.GetStringExtra ("FilePath");
			var bitmap = imageURL.LoadAndResizeBitmap (2048, 2048);
			imageViewExt.SetImageBitmap (bitmap);

			linearLayout.AddView (imageViewExt);
		}
	}
}

