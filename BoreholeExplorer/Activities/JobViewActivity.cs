using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using System.Net;
using Android.Content.PM;

namespace BoreholeExplorer
{
	[Activity (Label = "JobViewActivity", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class JobViewActivity : Activity, ListView.IOnItemClickListener
	{
		private JobData m_Job;
		private int m_JobIndex;
		private TextView m_JobNameView;
		private TextView m_JobDescriptionView;
		private ListView m_HoleListView;
		private SiteImageView m_JobSiteImageView;

		private ProgressDialog m_ProgressBar;
		private int m_UploadStage;
		private int m_TotalUploadStages;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.JobView);

			m_JobIndex = Intent.GetIntExtra ("JobIndex", 0);
			Console.WriteLine ("Got job index : " + m_JobIndex);
			m_Job = JobDataController.Instance.GetJobAtIndex (m_JobIndex);

			m_JobNameView = FindViewById<TextView> (Resource.Id.jvJobNameView);
			m_JobDescriptionView = FindViewById<TextView> (Resource.Id.jvJobDescriptionView);

			m_JobNameView.Text = m_Job.Name;
			m_JobDescriptionView.Text = m_Job.Description;
			m_JobDescriptionView.MovementMethod = new Android.Text.Method.ScrollingMovementMethod ();

			m_HoleListView = FindViewById<ListView> (Resource.Id.jvHoleListView);
			m_HoleListView.Adapter = new HoleListAdapter (this, m_Job.m_HoleDataList);
			m_HoleListView.OnItemClickListener = this;

			m_JobSiteImageView = FindViewById<SiteImageView> (Resource.Id.jvJobSiteImageView);
			m_JobSiteImageView.OnSizeChangedEvent += JobSizeImageViewChangedSizeHandler;

			JobDataController.Instance.SetCurrentJob (m_JobIndex);
		}

		private void JobSizeImageViewChangedSizeHandler() {
			Console.WriteLine ("JobSizeImageViewChangedSizeHandler");
			LoadJobSiteBitmap ();
		}

		private void LoadJobSiteBitmap() {
			Console.WriteLine ("Load Job Site Bitmap");

			var jobSiteImageFile = new Java.IO.File (JobDataController.Instance.GetSiteImagesDirectory (), JobDataController.Instance.GetSiteImageFilename (m_JobIndex, -1));

			if (jobSiteImageFile.Exists ()) {
				// we have a file - lets draw it
				Console.WriteLine ("Job Site Image View w:" + m_JobSiteImageView.m_TargetWidth + " h:" + m_JobSiteImageView.m_TargetHeight);
				m_Job.HasSiteImage = true;
				using (Bitmap bitmap = jobSiteImageFile.Path.LoadAndResizeBitmap (m_JobSiteImageView.m_TargetWidth, m_JobSiteImageView.m_TargetHeight)) {
					Console.WriteLine ("Image Loaded - yay!");
					m_JobSiteImageView.m_CurrentBitMap = jobSiteImageFile.Name;
					m_JobSiteImageView.SetImageBitmap (bitmap);
				}
			} else {
				m_Job.HasSiteImage = false;
			}

		}

		protected override void OnResume ()
		{
			base.OnResume ();

			// Refresh the text again
			m_JobNameView.Text = m_Job.Name;
			m_JobDescriptionView.Text = m_Job.Description;

			m_HoleListView.InvalidateViews ();

			LoadJobSiteBitmap ();
		}

		protected override void OnStop ()
		{
			base.OnStop ();
			Console.WriteLine ("On Stop");
			JobDataController.Instance.SaveJobDataToFiles ();
		}

		public void OnItemClick(AdapterView parent, View view, int position, long id)
		{
			var holeViewIntent = new Intent (this, typeof(HoleViewActivity));
			holeViewIntent.PutExtra ("JobIndex", m_JobIndex);
			holeViewIntent.PutExtra ("HoleIndex", position);
			StartActivity (holeViewIntent);
		}

		[Java.Interop.Export("OnEditJobButtonClick")] 
		public void OnEditJobButtonClick (View v)
		{
			Console.WriteLine ("Edit Job View");
			var editJobViewIntent = new Intent (this, typeof(EditJobViewActivity));
			editJobViewIntent.PutExtra ("JobIndex", m_JobIndex);
			StartActivity (editJobViewIntent);
		}

		[Java.Interop.Export("OnAddHoleButtonClick")] 
		public void OnAddHoleButtonClick (View v)
		{
			Console.WriteLine ("Add Hole View");
			var addHoleViewIntent = new Intent (this, typeof(EditHoleViewActivity));
			addHoleViewIntent.PutExtra ("JobIndex", m_JobIndex);
			addHoleViewIntent.PutExtra ("HoleIndex", -1);
			StartActivity (addHoleViewIntent);
		}

		[Java.Interop.Export("OnDeleteButtonClick")] 
		public void OnDeleteButtonClick (View v)
		{
			Console.WriteLine ("Delete Job");
			Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
			AlertDialog alertDialog = alertDialogBuilder.Create ();
			alertDialog.SetTitle ("Delete Job");
			alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
			alertDialog.SetMessage ("This will delete the Job and all of the Hole Data from this device. Are you Sure?");
			alertDialog.SetButton ("OK", (s, ev) => {
				// nothing to do in our case 
				Console.WriteLine("DELETE");
				JobDataController.Instance.DeleteJob(m_Job);
				Finish();
			});
			alertDialog.SetButton2 ("Cancel", (s, ev) => {
				// nothing to do in our case 
				Console.WriteLine("Cancel");
			});
			alertDialog.Show ();
		}

		[Java.Interop.Export("OnJobSiteImageClick")] 
		public void OnJobSiteImageClick (View v)
		{
			Console.WriteLine ("Job Site Image Click");
			var captureSiteImageViewIntent = new Intent (this, typeof(SiteImageCaptureActivity));
			captureSiteImageViewIntent.PutExtra ("JobIndex", m_JobIndex);
			captureSiteImageViewIntent.PutExtra ("HoleIndex", -1);
			StartActivity (captureSiteImageViewIntent);
		}

		[Java.Interop.Export("OnArchiveJobButtonClick")]
		public void OnArchiveJobButtonClick(View v)
		{
			if (!WiFiCheck.CheckForBoreholeSSID ()) {

				Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
				AlertDialog alertDialog = alertDialogBuilder.Create ();
				alertDialog.SetTitle ("Wrong Wi-Fi");
				alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
				alertDialog.SetMessage ("You do not have your WiFi Connected to \"" + WiFiCheck.SSIDCheckString + "\". Please setup your WiFi and retry the operation.");
				alertDialog.SetButton ("OK", (s, ev) => {
					// nothing to do in our case 
				});
				alertDialog.Show ();
				return;
			}

			m_Job.IsArchived = false;
			EmbeddedController.Instance.ArchiveDownloadCompletedEvent += ArchiveFinished;
			EmbeddedController.Instance.ArchiveDownloadErrorEvent += ArchiveError;
			EmbeddedController.Instance.ArchiveDownloadProgressChangedEvent += ArchiveProgressChanged;
			EmbeddedController.Instance.ArchiveJob ();

			m_ProgressBar = new ProgressDialog (v.Context);
			m_ProgressBar.SetCancelable (false);
			m_ProgressBar.SetMessage ("Downloading archive...");
			m_ProgressBar.SetProgressStyle (ProgressDialogStyle.Horizontal);
			m_ProgressBar.Progress = 0;
			m_ProgressBar.Max = 100;
			m_ProgressBar.Show ();
		}

		private void ArchiveProgressChanged(DownloadProgressChangedEventArgs e)
		{
			if(m_ProgressBar != null)
			{
				m_ProgressBar.Progress = e.ProgressPercentage;
			}
		}

		private void ArchiveError(string archiveName, string error)
		{
			m_Job.IsArchived = false;
			Console.WriteLine ("Archive error : " + archiveName + " : " + error);
			//EmbeddedController.Instance.ArchiveDownloadCompletedEvent -= ArchiveFinished;
			//EmbeddedController.Instance.ArchiveDownloadErrorEvent -= ArchiveError;
		}

		private void ArchiveFinished(string archiveName)
		{
			Console.WriteLine ("Archive downloaded : " + archiveName);
			EmbeddedController.Instance.ArchiveDownloadCompletedEvent -= ArchiveFinished;
			EmbeddedController.Instance.ArchiveDownloadErrorEvent -= ArchiveError;
			EmbeddedController.Instance.ArchiveDownloadProgressChangedEvent -= ArchiveProgressChanged;
			m_Job.IsArchived = true;
			m_ProgressBar.Dismiss ();
			m_ProgressBar = null;
		}

		[Java.Interop.Export("OnUploadJobButtonClick")]
		public void OnUploadJobButtonClick(View v)
		{
			if (WiFiCheck.CheckForBoreholeSSID ()) {

				Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
				AlertDialog alertDialog = alertDialogBuilder.Create ();
				alertDialog.SetTitle ("Wrong Wi-Fi");
				alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
				alertDialog.SetMessage ("You have your WiFi Connected to \"" + WiFiCheck.SSIDCheckString + "\". You need to disconnect from this network in order to upload to the server. Please setup your WiFi and retry the operation.");
				alertDialog.SetButton ("OK", (s, ev) => {
					// nothing to do in our case 
				});
				alertDialog.Show ();
				return;
			}

			m_Job.IsUploaded = false;
			FTPUploader.Instance.UploadCompletedEvent += UploadFinished;
			FTPUploader.Instance.UploadProgressChangedEvent += UploadProgressChanged;
			FTPUploader.Instance.UploadStageCompletedEvent += UploadStageCompleted;
			m_TotalUploadStages = FTPUploader.Instance.UploadJobNew ();
			m_UploadStage = 1;
			//FTPUploader.Instance.UploadJob ();

			m_ProgressBar = new ProgressDialog (v.Context);
			m_ProgressBar.SetCancelable (false);
			m_ProgressBar.SetMessage ("Uploading Part " + m_UploadStage + " of " + m_TotalUploadStages);
			m_ProgressBar.SetProgressStyle (ProgressDialogStyle.Horizontal);
			m_ProgressBar.Progress = 0;
			m_ProgressBar.Max = 100;
			m_ProgressBar.Show ();
		}

		private void UploadProgressChanged(int percent)
		{
			RunOnUiThread (() => {
				m_ProgressBar.Progress = percent;
			});
		}

		private void UploadStageCompleted( bool error )
		{
			Console.WriteLine("Upload Stage Completed - Error:" + error );
			RunOnUiThread (() => {
				if ( error ) {
					Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
					AlertDialog alertDialog = alertDialogBuilder.Create ();
					alertDialog.SetTitle ("Error Uploading");
					alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
					alertDialog.SetMessage ("the FTP Server may be down, please contact technical support.");
					alertDialog.SetButton ("OK", (s, ev) => {
						// nothing to do in our case 
					});
					alertDialog.Show ();
				} else {
					m_UploadStage++;
					m_ProgressBar.SetMessage ("Uploading Part " + m_UploadStage + " of " + m_TotalUploadStages);
				}
			});
		}

		private void UploadFinished()
		{
			FTPUploader.Instance.UploadCompletedEvent -= UploadFinished;
			m_Job.IsUploaded = true;
			RunOnUiThread (() => {
				m_ProgressBar.Dismiss ();
				m_ProgressBar = null;
			});
		}
	}
}

