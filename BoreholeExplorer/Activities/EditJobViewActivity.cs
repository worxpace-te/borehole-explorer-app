﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;

namespace BoreholeExplorer
{
	[Activity (Label = "AddJobViewActivity", ScreenOrientation = ScreenOrientation.Portrait)]
	public class EditJobViewActivity : Activity
	{
		private JobData m_Job;
		private int m_JobIndex;
		private bool m_AddJob;

		private TextView m_JobNameEditText;
		private TextView m_JobDescriptionEditText;

		private LinearLayout m_LinearLayoutThisSiteGrasses;
		private LinearLayout m_LinearLayoutThisSiteTrees;
		private LinearLayout m_LinearLayoutThisSiteStructures;

		private LinearLayout m_LinearLayoutAdjacentLeftPropertyGrasses;
		private LinearLayout m_LinearLayoutAdjacentLeftPropertyTrees;
		private LinearLayout m_LinearLayoutAdjacentLeftPropertyStructures;

		private LinearLayout m_LinearLayoutAdjacentRightPropertyGrasses;
		private LinearLayout m_LinearLayoutAdjacentRightPropertyTrees;
		private LinearLayout m_LinearLayoutAdjacentRightPropertyStructures;

		private LinearLayout m_LinearLayoutAdjacentRearPropertyGrasses;
		private LinearLayout m_LinearLayoutAdjacentRearPropertyTrees;
		private LinearLayout m_LinearLayoutAdjacentRearPropertyStructures;

		private LinearLayout m_LinearLayoutSlope;
		private LinearLayout m_LinearLayoutSurfaceConditions;
		private LinearLayout m_LinearLayoutSiteDrainage;

		private LinearLayout m_LinearLayoutWindRatingRegion;
		private LinearLayout m_LinearLayoutWindRatingTerrain;
		private LinearLayout m_LinearLayoutWindRatingShielding;
		private LinearLayout m_LinearLayoutWindRatingTopographic;
		private LinearLayout m_LinearLayoutWindRatingRating;

		/* unused
		private void CheckRadioButton(RadioGroup radioGroup, string radioButtonText)
		{
			radioGroup.ClearCheck();
			for (int c = 0; c < radioGroup.ChildCount; c++) {
				RadioButton b = (RadioButton)radioGroup.GetChildAt (c);
				if (b.Text == radioButtonText) {
					b.Checked = true;
					return;
				}
			}
		}

		private string SetRadioGroupText( RadioButton selectRadioButton ) 
		{
			string returnString = "Not Selected";
			if (selectRadioButton != null) {
				returnString = selectRadioButton.Text;
			}
			return returnString;
		}
		*/

		private void CheckCheckBoxes(LinearLayout checkboxLayout, List<string> checkboxTextList)
		{
			for (int c = 0; c < checkboxLayout.ChildCount; c++) {
				CheckBox b = (CheckBox)checkboxLayout.GetChildAt (c);
				b.Checked = false;
				foreach ( string checkBoxText in checkboxTextList ) {
					if (b.Text == checkBoxText ) {
						b.Checked = true;
						break;
					}
				}
			}
		}

		private List<string> SetCheckboxTextList( LinearLayout checkboxLayout ) 
		{
			List<string> returnStringList = new List<string>();
			for (int c = 0; c < checkboxLayout.ChildCount; c++) {
				CheckBox b = (CheckBox)checkboxLayout.GetChildAt (c);
				if (b.Checked) {
					returnStringList.Add (b.Text);
				}
			}

			return returnStringList;
		}

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.EditJobView);

			m_JobIndex = Intent.GetIntExtra ("JobIndex", 0);
			Console.WriteLine ("Got job index : " + m_JobIndex);
			if (m_JobIndex == -1) {
				// Create a new Job
				m_Job = new JobData ();
				m_Job.CreationDateTime = DateTime.Now;
				m_Job.Latitude = LocationController.Instance.GetLocationLatitude();
				m_Job.Longitude = LocationController.Instance.GetLocationLongitude();
				m_AddJob = true;
			} else {
				m_Job = JobDataController.Instance.GetJobAtIndex (m_JobIndex);
				m_AddJob = false;
			}

			// Get the veiw Edit Text Objects
			m_JobNameEditText = FindViewById<TextView> (Resource.Id.ajvJobNameEditText);
			m_JobDescriptionEditText = FindViewById<TextView> (Resource.Id.ajvJobDescriptionEditText);

			m_LinearLayoutThisSiteGrasses = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutThisSiteGrasses);
			m_LinearLayoutThisSiteTrees = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutThisSiteTrees);
			m_LinearLayoutThisSiteStructures = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutThisSiteStructures);

			m_LinearLayoutAdjacentLeftPropertyGrasses = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutAdjacentLeftPropertyGrasses);
			m_LinearLayoutAdjacentLeftPropertyTrees = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutAdjacentLeftPropertyTrees);
			m_LinearLayoutAdjacentLeftPropertyStructures = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutAdjacentLeftPropertyStructures);

			m_LinearLayoutAdjacentRightPropertyGrasses = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutAdjacentRightPropertyGrasses);
			m_LinearLayoutAdjacentRightPropertyTrees = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutAdjacentRightPropertyTrees);
			m_LinearLayoutAdjacentRightPropertyStructures = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutAdjacentRightPropertyStructures);

			m_LinearLayoutAdjacentRearPropertyGrasses = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutAdjacentRearPropertyGrasses);
			m_LinearLayoutAdjacentRearPropertyTrees = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutAdjacentRearPropertyTrees);
			m_LinearLayoutAdjacentRearPropertyStructures = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutAdjacentRearPropertyStructures);

			m_LinearLayoutSlope = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutSlope);
			m_LinearLayoutSurfaceConditions = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutSurfaceConditions);
			m_LinearLayoutSiteDrainage = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutSiteDrainage);

			m_LinearLayoutWindRatingRegion = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutWindRatingRegion);
			m_LinearLayoutWindRatingTerrain = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutWindRatingTerrain);
			m_LinearLayoutWindRatingShielding = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutWindRatingShielding);
			m_LinearLayoutWindRatingTopographic = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutWindRatingTopographic);
			m_LinearLayoutWindRatingRating = FindViewById<LinearLayout> (Resource.Id.ajvLinearLayoutWindRatingRating);




			// Get the text from the Job (will be empty if adding a new job - but that's ok)
			m_JobNameEditText.Text = m_Job.Name;
			m_JobDescriptionEditText.Text = m_Job.Description;

			CheckCheckBoxes (m_LinearLayoutThisSiteGrasses, m_Job.SiteData.SiteFeaturesThisSite.Grasses);
			CheckCheckBoxes (m_LinearLayoutThisSiteTrees, m_Job.SiteData.SiteFeaturesThisSite.Trees);
			CheckCheckBoxes (m_LinearLayoutThisSiteStructures, m_Job.SiteData.SiteFeaturesThisSite.Structures);

			CheckCheckBoxes (m_LinearLayoutAdjacentLeftPropertyGrasses, m_Job.SiteData.SiteFeaturesAdjacentLeftProperty.Grasses);
			CheckCheckBoxes (m_LinearLayoutAdjacentLeftPropertyTrees, m_Job.SiteData.SiteFeaturesAdjacentLeftProperty.Trees);
			CheckCheckBoxes (m_LinearLayoutAdjacentLeftPropertyStructures, m_Job.SiteData.SiteFeaturesAdjacentLeftProperty.Structures);

			CheckCheckBoxes (m_LinearLayoutAdjacentRightPropertyGrasses, m_Job.SiteData.SiteFeaturesAdjacentRightProperty.Grasses);
			CheckCheckBoxes (m_LinearLayoutAdjacentRightPropertyTrees, m_Job.SiteData.SiteFeaturesAdjacentRightProperty.Trees);
			CheckCheckBoxes (m_LinearLayoutAdjacentRightPropertyStructures, m_Job.SiteData.SiteFeaturesAdjacentRightProperty.Structures);

			CheckCheckBoxes (m_LinearLayoutAdjacentRearPropertyGrasses, m_Job.SiteData.SiteFeauturesAdjacentRearProperty.Grasses);
			CheckCheckBoxes (m_LinearLayoutAdjacentRearPropertyTrees, m_Job.SiteData.SiteFeauturesAdjacentRearProperty.Trees);
			CheckCheckBoxes (m_LinearLayoutAdjacentRearPropertyStructures, m_Job.SiteData.SiteFeauturesAdjacentRearProperty.Structures);

			CheckCheckBoxes (m_LinearLayoutSlope, m_Job.SiteData.Slope);
			CheckCheckBoxes (m_LinearLayoutSurfaceConditions, m_Job.SiteData.SurfaceConditions);
			CheckCheckBoxes (m_LinearLayoutSiteDrainage, m_Job.SiteData.SiteDrainage);

			CheckCheckBoxes (m_LinearLayoutWindRatingRegion, m_Job.SiteData.WindRating.Region);
			CheckCheckBoxes (m_LinearLayoutWindRatingTerrain, m_Job.SiteData.WindRating.Terrain);
			CheckCheckBoxes (m_LinearLayoutWindRatingShielding, m_Job.SiteData.WindRating.Shielding);
			CheckCheckBoxes (m_LinearLayoutWindRatingTopographic, m_Job.SiteData.WindRating.Topographic);
			CheckCheckBoxes (m_LinearLayoutWindRatingRating, m_Job.SiteData.WindRating.Rating);

		}

		private void DisplayAlert( string alertTitle, string alertMessage ) {
			Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
			AlertDialog alertDialog = alertDialogBuilder.Create ();
			alertDialog.SetTitle (alertTitle);
			alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
			alertDialog.SetMessage (alertMessage);
			alertDialog.SetButton ("OK", (s, ev) => {
				// nothing to do in our case 
			});
			alertDialog.Show ();
		}

		[Java.Interop.Export("OnSaveJobButtonClick")] 
		public void OnSaveJobButtonClick (View v)
		{
			// We need to check if the name is valid and if it already exists
			if (String.IsNullOrEmpty (m_JobNameEditText.Text)) {
				DisplayAlert ("Illegal Job Name", "Job Name cannot be empty");
				return;
			}

			m_JobNameEditText.Text = m_JobNameEditText.Text.Replace(" ", "_");

			// TODO: Add check here - if there is any hole data, we can't change the name of an existing project
			// should make the button grey out or something, so this never gets called
			if ( m_Job == null || m_Job.Name != m_JobNameEditText.Text ) {
				// we have changed the name - we are either adding a new job or have changed an existing one, so we need to check that the name does not already exist
				if (JobDataController.Instance.JobNameExists (m_JobNameEditText.Text)) {
					DisplayAlert ("Job Already Exists", "The Job name must be unique");
					return;
				}
			}

			// Save the job text changes - we do this here, and if the user presses the back button, the changes will be lost, as they should be
			m_Job.Name = m_JobNameEditText.Text;
			m_Job.Description = m_JobDescriptionEditText.Text;

			m_Job.SiteData.SiteFeaturesThisSite.Grasses = SetCheckboxTextList (m_LinearLayoutThisSiteGrasses);
			m_Job.SiteData.SiteFeaturesThisSite.Trees = SetCheckboxTextList (m_LinearLayoutThisSiteTrees);
			m_Job.SiteData.SiteFeaturesThisSite.Structures = SetCheckboxTextList (m_LinearLayoutThisSiteStructures);

			m_Job.SiteData.SiteFeaturesAdjacentLeftProperty.Grasses = SetCheckboxTextList (m_LinearLayoutAdjacentLeftPropertyGrasses);
			m_Job.SiteData.SiteFeaturesAdjacentLeftProperty.Trees = SetCheckboxTextList (m_LinearLayoutAdjacentLeftPropertyTrees);
			m_Job.SiteData.SiteFeaturesAdjacentLeftProperty.Structures = SetCheckboxTextList (m_LinearLayoutAdjacentLeftPropertyStructures);

			m_Job.SiteData.SiteFeaturesAdjacentRightProperty.Grasses = SetCheckboxTextList (m_LinearLayoutAdjacentRightPropertyGrasses);
			m_Job.SiteData.SiteFeaturesAdjacentRightProperty.Trees = SetCheckboxTextList (m_LinearLayoutAdjacentRightPropertyTrees);
			m_Job.SiteData.SiteFeaturesAdjacentRightProperty.Structures = SetCheckboxTextList (m_LinearLayoutAdjacentRightPropertyStructures);

			m_Job.SiteData.SiteFeauturesAdjacentRearProperty.Grasses = SetCheckboxTextList (m_LinearLayoutAdjacentRearPropertyGrasses);
			m_Job.SiteData.SiteFeauturesAdjacentRearProperty.Trees = SetCheckboxTextList (m_LinearLayoutAdjacentRearPropertyTrees);
			m_Job.SiteData.SiteFeauturesAdjacentRearProperty.Structures = SetCheckboxTextList (m_LinearLayoutAdjacentRearPropertyStructures);

			m_Job.SiteData.Slope = SetCheckboxTextList (m_LinearLayoutSlope);
			m_Job.SiteData.SurfaceConditions = SetCheckboxTextList (m_LinearLayoutSurfaceConditions);
			m_Job.SiteData.SiteDrainage = SetCheckboxTextList (m_LinearLayoutSiteDrainage);

			m_Job.SiteData.WindRating.Region = SetCheckboxTextList (m_LinearLayoutWindRatingRegion);
			m_Job.SiteData.WindRating.Terrain = SetCheckboxTextList (m_LinearLayoutWindRatingTerrain);
			m_Job.SiteData.WindRating.Shielding = SetCheckboxTextList (m_LinearLayoutWindRatingShielding);
			m_Job.SiteData.WindRating.Topographic = SetCheckboxTextList (m_LinearLayoutWindRatingTopographic);
			m_Job.SiteData.WindRating.Rating = SetCheckboxTextList (m_LinearLayoutWindRatingRating);

			if (m_AddJob) {
				Console.WriteLine ("Add/Edit New Job:" + m_Job.Name);
				JobDataController.Instance.AddJob (m_Job);
			}

			// Save the changes
			JobDataController.Instance.SaveJobDataToFiles ();

			// We are done - finish this activity
			Finish ();
		}

	}
}