﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;

namespace BoreholeExplorer
{
	[Activity (Label = "EditHoleViewActivity", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class EditHoleViewActivity : Activity
	{
		private JobData m_Job;
		private int m_JobIndex;
		private HoleData m_Hole;
		private int m_HoleIndex;
		private bool m_AddHole;

		private TextView m_HoleNameEditText;
		private TextView m_HoleDescriptionEditText;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.EditHoleView);

			m_JobIndex = Intent.GetIntExtra ("JobIndex", 0);
			Console.WriteLine ("Got job index : " + m_JobIndex);
			m_HoleIndex = Intent.GetIntExtra ("HoleIndex", 0);
			m_Job = JobDataController.Instance.GetJobAtIndex (m_JobIndex);
			if (m_HoleIndex == -1) {
				m_AddHole = true;
				// Create a new Hole
				m_Hole = new HoleData ();
				m_Hole.CreationDateTime = DateTime.Now;
				m_Hole.Latitude = LocationController.Instance.GetLocationLatitude();
				m_Hole.Longitude = LocationController.Instance.GetLocationLongitude();
			} else {
				m_AddHole = false;
				if (m_HoleIndex < 0 || m_HoleIndex >= m_Job.m_HoleDataList.Count) {
					Console.WriteLine ("Error: Hole index out of range : " + m_HoleIndex);

				}
				m_Hole = m_Job.m_HoleDataList [m_HoleIndex];
			}

			// Get the veiw Edit Text Objects
			var jobNameEditText = FindViewById<TextView> (Resource.Id.ehvJobNameTextView);
			m_HoleNameEditText = FindViewById<TextView> (Resource.Id.ehvHoleNameEditText);
			m_HoleDescriptionEditText = FindViewById<TextView> (Resource.Id.ehvHoleDescriptionEditText);

			// Get the text from the Hole (will be empty if adding a new hole - but that's ok)
			jobNameEditText.Text = m_Job.Name;
			m_HoleNameEditText.Text = m_Hole.Name;
			m_HoleDescriptionEditText.Text = m_Hole.Description;
		}

		private void DisplayAlert( string alertTitle, string alertMessage ) {
			Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
			AlertDialog alertDialog = alertDialogBuilder.Create ();
			alertDialog.SetTitle (alertTitle);
			alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
			alertDialog.SetMessage (alertMessage);
			alertDialog.SetButton ("OK", (s, ev) => {
				// nothing to do in our case 
			});
			alertDialog.Show ();
		}

		[Java.Interop.Export("OnSaveHoleButtonClick")] 
		public void OnSaveHoleButtonClick (View v)
		{
			// We need to check if the name is valid and if it already exists
			if (String.IsNullOrEmpty (m_HoleNameEditText.Text)) {
				DisplayAlert ("Illegal Hole Name", "Hole Name cannot be empty");
				return;
			}

			m_HoleNameEditText.Text = m_HoleNameEditText.Text.Replace(" ", "_");

			// TODO: Add check here - if there is any hole data, we can't change the name of an existing hole
			// should make the button grey out or something, so this never gets called
			if ( m_Hole.Name == null || m_Hole.Name != m_HoleNameEditText.Text ) {
				// we have changed the name - we are either adding a new job or have changed an existing one, so we need to check that the name does not already exist
				if (JobDataController.Instance.HoleNameExists (m_Job, m_HoleNameEditText.Text)) {
					DisplayAlert ("Hole Already Exists", "The Hole name must be unique");
					return;
				}
			}

			// Save the job text changes - we do this here, and if the user presses the back button, the changes will be lost, as they should be
			m_Hole.Name = m_HoleNameEditText.Text;
			m_Hole.Description = m_HoleDescriptionEditText.Text;

			if (m_AddHole) {
				Console.WriteLine ("Add New Hole:" + m_Hole.Name);
				JobDataController.Instance.AddHole (m_Job,m_Hole);
			}

			// Save the changes
			JobDataController.Instance.SaveJobDataToFiles ();

			// We are done - finish this activity
			Finish ();
		}

	}
}