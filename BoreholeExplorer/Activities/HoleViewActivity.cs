using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Util;
using Java.Lang;
using System.Threading.Tasks;
using Android.Content.PM;

namespace BoreholeExplorer
{
	public class BitmapLruCache : LruCache
	{
		public event Action<string> EntryRemovedEvent;
		public object m_LockObject = new object ();

		public BitmapLruCache (int cacheSize) : base(cacheSize)
		{

		}

		protected override int SizeOf (Java.Lang.Object key, Java.Lang.Object value)
		{
			var bitmap = value as Bitmap;
			if (bitmap == null)
				return -1;
			return bitmap.ByteCount / 1024;
		}

		protected override void EntryRemoved (bool evicted, Java.Lang.Object key, Java.Lang.Object oldValue, Java.Lang.Object newValue)
		{
			base.EntryRemoved (evicted, key, oldValue, newValue);
			Console.WriteLine ("Entry Removed : " + evicted + " : " + (string)key);
			if (EntryRemovedEvent != null)
				EntryRemovedEvent ((string)key);
		}
	}

	[Activity (Label = "HoleViewActivity", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class HoleViewActivity : Activity, ListView.IOnItemClickListener
	{
		private JobData m_Job;
		private int m_JobIndex;
		private HoleData m_Hole;
		private int m_HoleIndex;

		private TextView m_HoleNameView;
		private TextView m_HoleDescriptionView;
		private TextView m_HoleCreationView;
		private TextView m_HoleLatView;
		private TextView m_HoleLongView;

		private ListView m_HoleImageListView;
		private SiteImageView m_HoleSiteImageView;

		private BitmapLruCache m_MemoryCache;

		protected override void OnCreate (Bundle bundle)
		{
			Console.WriteLine("OnCreate");
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.HoleView);

			m_JobIndex = Intent.GetIntExtra ("JobIndex", 0);
			m_HoleIndex = Intent.GetIntExtra ("HoleIndex", 0);

			m_Job = JobDataController.Instance.GetJobAtIndex (m_JobIndex);
			if (m_HoleIndex < 0 || m_HoleIndex >= m_Job.m_HoleDataList.Count) {
				Console.WriteLine ("Error: Hole index out of range : " + m_HoleIndex);
			}
			m_Hole = m_Job.m_HoleDataList [m_HoleIndex];

			var jobNameView = FindViewById<TextView> (Resource.Id.hvJobNameTextView);
			jobNameView.Text = m_Job.Name;

			m_HoleNameView = FindViewById<TextView> (Resource.Id.hvHoleNameTextView);
			m_HoleNameView.Text = m_Hole.Name;
			m_HoleDescriptionView = FindViewById<TextView> (Resource.Id.hvHoleDescriptionTextView);
			m_HoleDescriptionView.Text = m_Hole.Description;
			m_HoleDescriptionView.MovementMethod = new Android.Text.Method.ScrollingMovementMethod ();
			m_HoleCreationView = FindViewById<TextView> (Resource.Id.hvCreationDateTextView);
			m_HoleCreationView.Text = m_Hole.CreationDateTime.ToString ();
			m_HoleLatView = FindViewById<TextView> (Resource.Id.hvLatitudeTextView);
			m_HoleLatView.Text = m_Hole.Latitude.ToString ();
			m_HoleLongView = FindViewById<TextView> (Resource.Id.hvLongitudeTextView);
			m_HoleLongView.Text = m_Hole.Longitude.ToString ();

			m_HoleSiteImageView = FindViewById<SiteImageView> (Resource.Id.hvHoleSiteImageView);
			m_HoleSiteImageView.OnSizeChangedEvent += HoleSizeImageViewChangedSizeHandler;

			m_HoleImageListView = FindViewById<ListView> (Resource.Id.holeImageListView);
			var adapter = new HoleImageListAdapter (this, m_Job.m_HoleDataList [m_HoleIndex].m_HoleDataImages);
			adapter.SetHoleViewActivity (this);
			m_HoleImageListView.Adapter = adapter;
			m_HoleImageListView.OnItemClickListener = this;

			JobDataController.Instance.SetCurrentHole (m_HoleIndex);

			// Get max available VM memory, exceeding this amount will throw an OutOfMemroy exception.
			// Stored in kilobytes as LruCache takes an int in its constructor
			int maxMemory = (int)(Runtime.GetRuntime ().MaxMemory () / 1024);

			// Use 1/8th of the available memory for this memory cche.
			int cacheSize = maxMemory / 8;
			m_MemoryCache = new BitmapLruCache (cacheSize);
		}


		public void AddBitmapToMemoryCache(string key, Bitmap bitmap)
		{
			lock (m_MemoryCache.m_LockObject) {
				if (m_MemoryCache.Get (key) as Bitmap == null) {
					m_MemoryCache.Put (key, bitmap);
				}
			}
		}

		public Bitmap GetBitmapFromMemCache(string key) {
			Bitmap bitmap;
			lock (m_MemoryCache.m_LockObject) {
				bitmap = m_MemoryCache.Get (key) as Bitmap;
			}
			return bitmap;
		}

		public void LoadBitmapIntoImageView(string bitmapKey, SiteImageView imageView)
		{
			var bitmap = GetBitmapFromMemCache (bitmapKey);
			if (bitmap != null) {
				RunOnUiThread (() => {
					imageView.SetImageBitmap (bitmap);
				});
			} else {
				LoadBitmap (bitmapKey, imageView);
			}
		}

		private async void LoadBitmap(string filePath, SiteImageView imageView)
		{
			try {
				var options = new BitmapFactory.Options {
					InJustDecodeBounds = true,
				};
				Task<Bitmap> t = BitmapFactory.DecodeFileAsync (filePath, options);
				using (var dispose = await t) {
				}
				var imageHeight = options.OutHeight;
				var imageWidth = options.OutWidth;
				//var ratio = (float)imageView.m_TargetWidth / (float)imageWidth; // Commented out as it isn't used.
				Task<Bitmap> t2 = filePath.LoadAndResizeBitmapToWidthAsync (imageView.m_TargetWidth);
				var bitmap = await t2;
				AddBitmapToMemoryCache (filePath, bitmap);
				RunOnUiThread (() => {
					imageView.SetImageBitmap (bitmap);
				});
			}
			catch ( System.Exception e ) {
				Console.WriteLine ("LoadBitmap Exception:" + e.Message );
			}
		}

		private void HoleSizeImageViewChangedSizeHandler() {
			Console.WriteLine ("JobSizeImageViewChangedSizeHandler");
			LoadHoleSiteBitmap ();
		}

		private void LoadHoleSiteBitmap() {
			Console.WriteLine ("Load Job Site Bitmap");

			var jobSiteImageFile = new Java.IO.File (JobDataController.Instance.GetSiteImagesDirectory (), JobDataController.Instance.GetSiteImageFilename (m_JobIndex, m_HoleIndex));

			if (jobSiteImageFile.Exists ()) {
				// we have a file - lets draw it
				Console.WriteLine ("Job Site Image View w:" + m_HoleSiteImageView.m_TargetWidth + " h:" + m_HoleSiteImageView.m_TargetHeight);
				using (Bitmap bitmap = jobSiteImageFile.Path.LoadAndResizeBitmap (m_HoleSiteImageView.m_TargetWidth, m_HoleSiteImageView.m_TargetHeight)) {
					Console.WriteLine ("Image Loaded - yay!");
					m_HoleSiteImageView.m_CurrentBitMap = jobSiteImageFile.Name;
					m_HoleSiteImageView.SetImageBitmap (bitmap);
				}
				m_Hole.HasSiteImage = true;
			} else {
				// We don't have one
				m_Hole.HasSiteImage = false;
			}
		}

		private void PollingImageListChanged(List<string> imageURLs)
		{
			bool newImages = false;
			Console.WriteLine ("Images : " + imageURLs.Count);

			RunOnUiThread (() => {
				for (int i = 0; i < imageURLs.Count; i++) {
					var holeDataImage = JobDataController.Instance.AddHoleImageURL (this, "Image " + i, imageURLs [i]);
					if (holeDataImage != null) {
						newImages = true;
					}
				}
				if(newImages) {
					((HoleImageListAdapter)m_HoleImageListView.Adapter).NotifyDataSetChanged ();
					m_Hole.HasHoleImages = true;
				}
			});
		}

		protected override void OnStart ()
		{
			base.OnStart ();
			EmbeddedController.Instance.ImageURLsUpdatedEvent += PollingImageListChanged;
			EmbeddedController.Instance.BeginPollingCurrentHole ();
		}

		protected override void OnResume ()
		{
			base.OnResume ();

			m_HoleNameView.Text = m_Hole.Name;
			m_HoleDescriptionView.Text = m_Hole.Description;
			m_HoleCreationView.Text = m_Hole.CreationDateTime.ToString ();
			m_HoleLatView.Text = m_Hole.Latitude.ToString ();
			m_HoleLongView.Text = m_Hole.Longitude.ToString ();

			LoadHoleSiteBitmap ();
		}

		protected override void OnStop ()
		{
			base.OnStop ();
			Console.WriteLine ("On Stop");
			EmbeddedController.Instance.ImageURLsUpdatedEvent -= PollingImageListChanged;
			EmbeddedController.Instance.StopCurrentPolling ();
			EmbeddedController.Instance.ClearImageURLS ();
			JobDataController.Instance.SaveJobDataToFiles ();
		}

		public void OnItemClick (AdapterView parent, View view, int position, long id)
		{
			var intent = new Intent (this, typeof(ScalingImageActivity));
			intent.PutExtra ("FilePath", m_Hole.m_HoleDataImages [position].FilePath);
			StartActivity (intent);
		}

		[Java.Interop.Export("OnEditHoleButtonClick")]
		public void OnEditHoleButtonClick(View v)
		{
			Console.WriteLine ("Add Hole View");
			var editHoleViewIntent = new Intent (this, typeof(EditHoleViewActivity));
			editHoleViewIntent.PutExtra ("JobIndex", m_JobIndex);
			editHoleViewIntent.PutExtra ("HoleIndex", m_HoleIndex);
			StartActivity (editHoleViewIntent);
		}

		[Java.Interop.Export("OnHoleSiteImageClick")] 
		public void OnHoleSiteImageClick (View v)
		{
			Console.WriteLine ("Hole Site Image Click");
			var captureSiteImageViewIntent = new Intent (this, typeof(SiteImageCaptureActivity));
			captureSiteImageViewIntent.PutExtra ("JobIndex", m_JobIndex);
			captureSiteImageViewIntent.PutExtra ("HoleIndex", m_HoleIndex);
			StartActivity (captureSiteImageViewIntent);
		}

		[Java.Interop.Export("OnClickUpButton")]
		public void OnClickUpButton(View v)
		{
			Console.WriteLine ("OnClickUpButton");

			if (!WiFiCheck.CheckForBoreholeSSID ()) {

				Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
				AlertDialog alertDialog = alertDialogBuilder.Create ();
				alertDialog.SetTitle ("Wrong Wi-Fi");
				alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
				alertDialog.SetMessage ("You do not have your WiFi Connected to \"" + WiFiCheck.SSIDCheckString + "\". Please setup your WiFi and retry the operation.");
				alertDialog.SetButton ("OK", (s, ev) => {
					// nothing to do in our case 
				});
				alertDialog.Show ();
				return;
			}

			Console.WriteLine ("MoveCameraUp");
			EmbeddedController.Instance.MoveCameraUp ();
		}

		[Java.Interop.Export("OnClickDownButton")]
		public void OnClickDownButton(View v)
		{
			Console.WriteLine ("OnClickDownButton");

			if (!WiFiCheck.CheckForBoreholeSSID ()) {

				Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
				AlertDialog alertDialog = alertDialogBuilder.Create ();
				alertDialog.SetTitle ("Wrong Wi-Fi");
				alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
				alertDialog.SetMessage ("You do not have your WiFi Connected to \"" + WiFiCheck.SSIDCheckString + "\". Please setup your WiFi and retry the operation.");
				alertDialog.SetButton ("OK", (s, ev) => {
					// nothing to do in our case 
				});
				alertDialog.Show ();
				return;
			}

			Console.WriteLine ("MoveCameraDown");
			EmbeddedController.Instance.MoveCameraDown ();
		}

		[Java.Interop.Export("OnClickStartButton")]
		public void OnClickStartButton(View v)
		{
			Console.WriteLine ("OnClickStartButton");

			if (!WiFiCheck.CheckForBoreholeSSID ()) {

				Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
				AlertDialog alertDialog = alertDialogBuilder.Create ();
				alertDialog.SetTitle ("Wrong Wi-Fi");
				alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
				alertDialog.SetMessage ("You do not have your WiFi Connected to \"" + WiFiCheck.SSIDCheckString + "\". Please setup your WiFi and retry the operation.");
				alertDialog.SetButton ("OK", (s, ev) => {
					// nothing to do in our case 
				});
				alertDialog.Show ();
				return;
			}

			// Resend the job and hole names to the embeded deivce in case we were not on the wi fi network when we naviagted here
			Console.WriteLine ("ResendJobAndHoleNames");
			if (!EmbeddedController.Instance.ResendJobAndHoleNames ()) {
				Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
				AlertDialog alertDialog = alertDialogBuilder.Create ();
				alertDialog.SetTitle ("Error null Job or Hole");
				alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
				alertDialog.SetMessage ("Try creating a new Job and Hole and if the problem persists, please report to HartGeo Technical Support.");
				alertDialog.SetButton ("OK", (s, ev) => {
					// nothing to do in our case 
				});
				alertDialog.Show ();
				return;
			}

			Console.WriteLine ("StartRun");
			EmbeddedController.Instance.StartRun ();
		}

		[Java.Interop.Export("OnClickStopButton")]
		public void OnClickStopButton(View v)
		{
			Console.WriteLine ("OnClickStopButton");

			if (!WiFiCheck.CheckForBoreholeSSID ()) {

				Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
				AlertDialog alertDialog = alertDialogBuilder.Create ();
				alertDialog.SetTitle ("Wrong Wi-Fi");
				alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
				alertDialog.SetMessage ("You do not have your WiFi Connected to \"" + WiFiCheck.SSIDCheckString + "\". Please setup your WiFi and retry the operation.");
				alertDialog.SetButton ("OK", (s, ev) => {
					// nothing to do in our case 
				});
				alertDialog.Show ();
				return;
			}

			Console.WriteLine ("StopCamera");
			EmbeddedController.Instance.StopCamera ();

		}

		[Java.Interop.Export("OnClickRestartButton")]
		public void OnClickRestartButton(View v)
		{
			Console.WriteLine ("OnClickRestartButton");

			if (!WiFiCheck.CheckForBoreholeSSID ()) {

				Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
				AlertDialog alertDialog = alertDialogBuilder.Create ();
				alertDialog.SetTitle ("Wrong Wi-Fi");
				alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
				alertDialog.SetMessage ("You do not have your WiFi Connected to \"" + WiFiCheck.SSIDCheckString + "\". Please setup your WiFi and retry the operation.");
				alertDialog.SetButton ("OK", (s, ev) => {
					// nothing to do in our case 
				});
				alertDialog.Show ();
				return;
			}

			Console.WriteLine ("RestartCamera");
			EmbeddedController.Instance.RestartCamera ();
		}

		[Java.Interop.Export("OnClickRetrieveButton")]
		public void OnClickRetrieveButton(View v)
		{
			Console.WriteLine ("OnClickRetrieveButton");

			if (!WiFiCheck.CheckForBoreholeSSID ()) {

				Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
				AlertDialog alertDialog = alertDialogBuilder.Create ();
				alertDialog.SetTitle ("Wrong Wi-Fi");
				alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
				alertDialog.SetMessage ("You do not have your WiFi Connected to \"" + WiFiCheck.SSIDCheckString + "\". Please setup your WiFi and retry the operation.");
				alertDialog.SetButton ("OK", (s, ev) => {
					// nothing to do in our case 
				});
				alertDialog.Show ();
				return;
			}

			Console.WriteLine ("RecoverCamera");
			EmbeddedController.Instance.RecoverCamera ();
		}
	}
}

