﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using System.Net;
using Android.Content.PM;

namespace BoreholeExplorer
{
	[Activity (Label = "Borehole Explorer", MainLauncher = true, ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : Activity, ListView.IOnItemClickListener
	{
		private List<JobData> m_JobData;
		private ListView m_JobListView;

		private JobData m_ArchiveJob;
		private ProgressDialog m_ProgressBar;
		private View m_ProgressView;

		private JobData m_UploadJob;
		private int m_UploadStage;
		private int m_TotalUploadStages;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			m_JobListView = FindViewById<ListView> (Resource.Id.mainJobListView);
			m_JobData = JobDataController.Instance.GetJobDataList ();
			m_JobListView.Adapter = new JobListAdapter (this, m_JobData);
			m_JobListView.OnItemClickListener = this;

			EmbeddedController.Instance.Init ();

			LocationController.Instance.Setup (this.ApplicationContext);
		}

		protected override void OnResume ()
		{
			base.OnResume ();

			// Refresh the List view in case the data has changed since we were last active
			m_JobListView.InvalidateViews ();
			// Set the Current Job to None
			JobDataController.Instance.SetCurrentJob (-1);
		}

		public void OnItemClick (AdapterView parent, View view, int position, long id)
		{
			var jobViewIntent = new Intent (this, typeof(JobViewActivity));
			jobViewIntent.PutExtra ("JobIndex", position);
			StartActivity (jobViewIntent);
		}

		[Java.Interop.Export("OnAddJobButtonClick")] 
		public void OnAddJobButtonClick (View v)
		{
			var editJobViewIntent = new Intent (this, typeof(EditJobViewActivity));
			editJobViewIntent.PutExtra ("JobIndex", -1); // -1 means add a new job
			StartActivity (editJobViewIntent);
		}

		[Java.Interop.Export("OnArchiveAllButtonClick")] 
		public void OnArchiveAllButtonClick (View v)
		{
			Console.WriteLine ("OnArchiveAllButtonClick");
			if (!WiFiCheck.CheckForBoreholeSSID ()) {

				Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
				AlertDialog alertDialog = alertDialogBuilder.Create ();
				alertDialog.SetTitle ("Wrong Wi-Fi");
				alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
				alertDialog.SetMessage ("You do not have your WiFi Connected to \"" + WiFiCheck.SSIDCheckString + "\". Please setup your WiFi and retry the operation.");
				alertDialog.SetButton ("OK", (s, ev) => {
					// nothing to do in our case 
				});
				alertDialog.Show ();
				return;
			}

			m_ProgressView = v;

			ArchiveCheckAndStart ();

		}

		private void ArchiveCheckAndStart() {
			for (int jobIndex=0; jobIndex<m_JobData.Count; jobIndex++ ) {
				Console.WriteLine ("Check Job:" + m_JobData[jobIndex].Name + " - Completed:" + m_JobData[jobIndex].IsCompleted + " - Archived:" + m_JobData[jobIndex].IsArchived );
				if (m_JobData[jobIndex].IsCompleted && !m_JobData[jobIndex].IsArchived) {
					m_ArchiveJob = m_JobData [jobIndex];
					Console.WriteLine ("Archive:" + m_ArchiveJob.Name);
					JobDataController.Instance.SetCurrentJob (jobIndex);
					m_JobData[jobIndex].IsArchived = false;
					EmbeddedController.Instance.ArchiveDownloadCompletedEvent += ArchiveFinished;
					EmbeddedController.Instance.ArchiveDownloadErrorEvent += ArchiveError;
					EmbeddedController.Instance.ArchiveDownloadProgressChangedEvent += ArchiveProgressChanged;
					EmbeddedController.Instance.ArchiveJob ();

					RunOnUiThread (() => {
						m_ProgressBar = new ProgressDialog (m_ProgressView.Context);
						m_ProgressBar.SetCancelable (false);
						m_ProgressBar.SetMessage ("Downloading archive : " + m_ArchiveJob.Name);
						m_ProgressBar.SetProgressStyle (ProgressDialogStyle.Horizontal);
						m_ProgressBar.Progress = 0;
						m_ProgressBar.Max = 100;
						m_ProgressBar.Show ();
					});
					return;
				}
			}
		}

		private void ArchiveProgressChanged(DownloadProgressChangedEventArgs e)
		{
			if(m_ProgressBar != null)
			{
				m_ProgressBar.Progress = e.ProgressPercentage;
			}
		}

		private void ArchiveError(string archiveName, string error)
		{
			m_ArchiveJob.IsArchived = false;
			Console.WriteLine ("Archive error : " + archiveName + " : " + error);
			//EmbeddedController.Instance.ArchiveDownloadCompletedEvent -= ArchiveFinished;
			//EmbeddedController.Instance.ArchiveDownloadErrorEvent -= ArchiveError;
		}

		private void ArchiveFinished(string archiveName)
		{
			Console.WriteLine ("Archive downloaded : " + archiveName);
			RunOnUiThread (() => {
				EmbeddedController.Instance.ArchiveDownloadCompletedEvent -= ArchiveFinished;
				EmbeddedController.Instance.ArchiveDownloadErrorEvent -= ArchiveError;
				EmbeddedController.Instance.ArchiveDownloadProgressChangedEvent -= ArchiveProgressChanged;
				m_ArchiveJob.IsArchived = true;
				m_ProgressBar.Dismiss ();
				m_ProgressBar = null;

				// Keep checking until they are all done
				ArchiveCheckAndStart ();
			});
		}


		[Java.Interop.Export("OnUploadAllButtonClick")] 
		public void OnUploadAllButtonClick (View v)
		{
			Console.WriteLine ("OnUploadAllButtonClick");
			if (WiFiCheck.CheckForBoreholeSSID ()) {

				Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
				AlertDialog alertDialog = alertDialogBuilder.Create ();
				alertDialog.SetTitle ("Wrong Wi-Fi");
				alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
				alertDialog.SetMessage ("You have your WiFi Connected to \"" + WiFiCheck.SSIDCheckString + "\". You need to disconnect from this network in order to upload to the server. Please setup your WiFi and retry the operation.");
				alertDialog.SetButton ("OK", (s, ev) => {
					// nothing to do in our case 
				});
				alertDialog.Show ();
				return;
			}

			m_ProgressView = v;

			UploadCheckAndStart ();

		}

		private void UploadCheckAndStart() {

			for (int jobIndex=0; jobIndex<m_JobData.Count; jobIndex++ ) {
				Console.WriteLine ("Check Job:" + m_JobData[jobIndex].Name + " - Completed:" + m_JobData[jobIndex].IsCompleted + " - Archived:" + m_JobData[jobIndex].IsArchived + " - Uploaded:" + m_JobData[jobIndex].IsUploaded );
				if (m_JobData[jobIndex].IsCompleted && m_JobData[jobIndex].IsArchived && !m_JobData[jobIndex].IsUploaded) {
					m_UploadJob = m_JobData [jobIndex];
					Console.WriteLine ("Upload:" + m_UploadJob.Name);
					JobDataController.Instance.SetCurrentJob (jobIndex);
					m_UploadJob.IsUploaded = false;

					FTPUploader.Instance.UploadCompletedEvent += UploadFinished;
					FTPUploader.Instance.UploadProgressChangedEvent += UploadProgressChanged;
					FTPUploader.Instance.UploadStageCompletedEvent += UploadStageCompleted;
					m_TotalUploadStages = FTPUploader.Instance.UploadJobNew ();
					m_UploadStage = 1;
					//FTPUploader.Instance.UploadJob ();

					RunOnUiThread (() => {
						m_ProgressBar = new ProgressDialog (m_ProgressView.Context);
						m_ProgressBar.SetCancelable (false);
						m_ProgressBar.SetMessage ("Uploading Part " + m_UploadStage + " of " + m_TotalUploadStages);
						m_ProgressBar.SetProgressStyle (ProgressDialogStyle.Horizontal);
						m_ProgressBar.Progress = 0;
						m_ProgressBar.Max = 100;
						m_ProgressBar.Show ();
					});
					return;
				}
			}

		}

		private void UploadProgressChanged(int percent)
		{
			RunOnUiThread (() => {
				m_ProgressBar.Progress = percent;
			});
		}

		private void UploadStageCompleted( bool error )
		{
			Console.WriteLine("Upload Stage Completed - Error:" + error );
			RunOnUiThread (() => {
				if ( error ) {
					Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
					AlertDialog alertDialog = alertDialogBuilder.Create ();
					alertDialog.SetTitle ("Error Uploading");
					alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
					alertDialog.SetMessage ("the FTP Server may be down, please contact technical support.");
					alertDialog.SetButton ("OK", (s, ev) => {
						// nothing to do in our case 
					});
					alertDialog.Show ();
				} else {
					m_UploadStage++;
					m_ProgressBar.SetMessage ("Uploading Part " + m_UploadStage + " of " + m_TotalUploadStages + " for job : " + JobDataController.Instance.CurrentJob.Name);
				}
			});
		}

		private void UploadFinished()
		{
			FTPUploader.Instance.UploadCompletedEvent -= UploadFinished;
			FTPUploader.Instance.UploadProgressChangedEvent -= UploadProgressChanged;
			FTPUploader.Instance.UploadStageCompletedEvent -= UploadStageCompleted;
			m_UploadJob.IsUploaded = true;
			RunOnUiThread (() => {
				m_ProgressBar.Dismiss ();
				m_ProgressBar = null;
			});

			UploadCheckAndStart();
		}


		[Java.Interop.Export("OnShutdownButtonClick")] 
		public void OnShutdownButtonClick (View v)
		{
			if (!WiFiCheck.CheckForBoreholeSSID ()) {

				Android.App.AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
				AlertDialog alertDialog = alertDialogBuilder.Create ();
				alertDialog.SetTitle ("Wrong Wi-Fi");
				alertDialog.SetIcon (Android.Resource.Drawable.IcDialogAlert);
				alertDialog.SetMessage ("You do not have your WiFi Connected to \"" + WiFiCheck.SSIDCheckString + "\". Please setup your WiFi and retry the operation.");
				alertDialog.SetButton ("OK", (s, ev) => {
					// nothing to do in our case 
				});
				alertDialog.Show ();
				return;
			}

			Console.WriteLine ("OnShutdownButtonClick");
			EmbeddedController.Instance.Shutdown ();
		}
	}
}
