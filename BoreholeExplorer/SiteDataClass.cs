﻿using System;
using System.Collections.Generic;

namespace BoreholeExplorer
{
	public class SiteDataClass
	{
		public SiteFeatures SiteFeaturesThisSite { get; set; }
		public SiteFeatures SiteFeaturesAdjacentLeftProperty { get; set; }
		public SiteFeatures SiteFeaturesAdjacentRightProperty { get; set; }
		public SiteFeatures SiteFeauturesAdjacentRearProperty { get; set; }
		public List<string> Slope { get; set; }
		public List<string> SurfaceConditions { get; set; }
		public List<string> SiteDrainage { get; set; }
		public WindRatingClass WindRating { get; set; }

		public SiteDataClass ()
		{
			SiteFeaturesThisSite = new SiteFeatures ();
			SiteFeaturesAdjacentLeftProperty = new SiteFeatures ();
			SiteFeaturesAdjacentRightProperty = new SiteFeatures ();
			SiteFeauturesAdjacentRearProperty = new SiteFeatures ();
			Slope = new List<string> ();
			SurfaceConditions = new List<string> ();
			SiteDrainage = new List<string> ();
			WindRating = new WindRatingClass ();
		}
	}
}

