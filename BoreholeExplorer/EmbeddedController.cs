﻿using System;
using System.Net;
using System.IO;
using System.Timers;
using System.Collections.Generic;

namespace BoreholeExplorer
{
	public class EmbeddedController
	{
		#region Singleton
		private static EmbeddedController m_Instance;

		public static EmbeddedController Instance 
		{
			get {
				if (null == m_Instance) {
					m_Instance = new EmbeddedController ();
				}
				return m_Instance;
			}
		}
		#endregion

		private JobDataController m_JDC;

		private const string m_UrlBase = "http://192.168.1.147/";
		private const string m_ScriptName = "cgi-bin/start.sh";
		private const string m_JobsFolder = "jobs/";

		private const int m_PollTime = 5000;
		private Timer m_HoleListPollingTimer;
		private string m_PollingJobName;
		private string m_PollingHoleName;
		private WebClient m_CurrentHoleListPollingClient;
		private string m_PollingList;
		public event Action<List<string>> ImageURLsUpdatedEvent;
		private List<string> m_ImageURLs = new List<string>();

		private EmbeddedController () 
		{
			ArchiveDirectory = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments), "Archives");
			if(!Directory.Exists(ArchiveDirectory)) {
				Directory.CreateDirectory (ArchiveDirectory);
			}
		}

		~EmbeddedController()
		{
			if (m_JDC != null) {
				m_JDC.CurrentJobChanged -= CurrentJobChanged;
				m_JDC.CurrentHoleChanged -= CurrentHoleChanged;
			}
		}

		public void Init()
		{
			m_JDC = JobDataController.Instance;
			m_JDC.CurrentJobChanged += CurrentJobChanged;
			m_JDC.CurrentHoleChanged += CurrentHoleChanged;
		}

		private void CurrentJobChanged()
		{
			var job = m_JDC.CurrentJob;
			if (null == job) {
				// Do nothing?
				return;
			}
			SetJobName (job.Name);
		}

		private void CurrentHoleChanged()
		{
			var hole = m_JDC.CurrentHole;
			if (null == hole) {
				// Do Nothing?
				return;
			}
			SetHoleName (hole.Name);
		}

		private string SetJobName(string jobName)
		{
			var fullURL = Uri.EscapeUriString( m_UrlBase + m_ScriptName + "?job_name=" + jobName);
			return SendWebRequest (fullURL);
		}

		private string SetHoleName(string holeName)
		{
			var fullURL = Uri.EscapeUriString (m_UrlBase + m_ScriptName + "?hole_name=" + holeName);
			return SendWebRequest (fullURL);
		}

		public bool ResendJobAndHoleNames() {
			var job = m_JDC.CurrentJob;
			if (null == job) {
				// Do nothing?
				return false;
			}
			SetJobName (job.Name);

			var hole = m_JDC.CurrentHole;
			if (null == hole) {
				// Do Nothing?
				return false;
			}
			SetHoleName (hole.Name);

			return true;
		}

		/* unused
		private void SetJobLocationDescription(string description)
		{
			var url = m_UrlBase + m_ScriptName + "?job_location=" + description;
			SendWebRequest (url);
		}

		private void SetHoleLocationDescription(string description)
		{
			var url = m_UrlBase + m_ScriptName + "?hole_location=" + description;
			SendWebRequest (url);	
		}
		*/

		public void BeginPollingCurrentHole()
		{
			BeginPollingHoleList (m_JDC.CurrentJob.Name, m_JDC.CurrentHole.Name);
		}

		private void BeginPollingHoleList(string jobName, string holeName)
		{
			Console.WriteLine ("Beginning Polling : " + jobName + " : " + holeName);
			if (null == m_HoleListPollingTimer) {
				m_HoleListPollingTimer = new Timer (m_PollTime);
				m_HoleListPollingTimer.Elapsed += new ElapsedEventHandler (OnPollingEvent);
			}
			m_PollingJobName = jobName;
			m_PollingHoleName = holeName;
			m_HoleListPollingTimer.Enabled = true;
			// Call it once straight away
			OnPollingEvent (null, null);
		}

		public void StopCurrentPolling()
		{
			m_HoleListPollingTimer.Enabled = false;
		}

		private void OnPollingEvent(object source, ElapsedEventArgs e)
		{
			//Console.WriteLine ("POLLING EVENT : " + m_UrlBase + m_JobsFolder + m_PollingJobName + "/" + m_PollingHoleName + "/frames.txt");
			SendPollingWebRequest(Uri.EscapeUriString(m_UrlBase + m_JobsFolder + m_PollingJobName + "/" + m_PollingHoleName + "/frames.txt"));
		}

		private void SendPollingWebRequest(string url)
		{
			if (m_CurrentHoleListPollingClient != null) {
				m_CurrentHoleListPollingClient.CancelAsync ();
				m_CurrentHoleListPollingClient = null;
			}
			m_CurrentHoleListPollingClient = new WebClient ();
			m_CurrentHoleListPollingClient.DownloadStringCompleted += (sender, e) => {
				if(e.Cancelled)
					return;
				if(e.Error != null)
					return;
				m_PollingList = e.Result;
				//Console.WriteLine("Got List : " + m_PollingList);
				ParsePollingList();
			};
			m_CurrentHoleListPollingClient.DownloadStringAsync (new Uri(url));
		}

		private string SendWebRequest(string url)
		{
			if (!WiFiCheck.CheckForBoreholeSSID ()) {
				return "Not conencted to " + WiFiCheck.SSIDCheckString;
			}

			WebClient client = new WebClient ();
			try
			{
				Console.WriteLine("Sending Web request : " + url);
				var s = client.DownloadString (url);
				Console.WriteLine ("Request Response : " + s);
				return s;
			}
			catch(Exception e) {
				Console.WriteLine ("Error with Web Request : " + e.Message);
				return "";
			}
		}

		public void Shutdown()
		{
			var url = m_UrlBase + m_ScriptName + "?shutdown=1";
			SendWebRequest (url);
		}

		public void StartRun()
		{
			var url = m_UrlBase + m_ScriptName + "?run=1";
			SendWebRequest (url);
		}

		public void StopCamera()
		{
			var url = m_UrlBase + m_ScriptName + "?camera_stop=1";
			SendWebRequest (url);
		}

		public void RestartCamera()
		{
			var url = m_UrlBase + m_ScriptName + "?camera_restart=1";
			SendWebRequest (url);
		}

		public void RecoverCamera()
		{
			var url = m_UrlBase + m_ScriptName + "?camera_recover=1";
			SendWebRequest (url);
		}

		public void MoveCameraUp(int amount = 1)
		{
			var url = m_UrlBase + m_ScriptName + "?camera_up=" + amount;
			SendWebRequest (url);
		}

		public void MoveCameraDown(int amount = 1)
		{
			var url = m_UrlBase + m_ScriptName + "?camera_down=" + amount;
			SendWebRequest (url);
		}

		public void SetFocus(int focusLevel = 180)
		{
			var url = m_UrlBase + m_ScriptName + "focus_set?=1";
			SendWebRequest (url);
		}

		public void FocusUp()
		{
			var url = m_UrlBase + m_ScriptName + "?focus_up=1";
			SendWebRequest (url);
		}

		public void FocusDown()
		{
			var url = m_UrlBase + m_ScriptName + "?focus_down=1";
			SendWebRequest (url);
		}

		private void ParsePollingList()
		{
			var imagePaths = m_PollingList.Split('\n', '\r');
			bool updated = false;
			for (int i = 0; i < imagePaths.Length; i++) {
				if (string.IsNullOrEmpty (imagePaths [i]))
					continue;
				// Remove leading ../
				var path = imagePaths [i].Remove (0, 9);
				var url = m_UrlBase + path;
				if (!m_ImageURLs.Contains (url)) {
					Console.WriteLine ("Add URL:" + url);
					m_ImageURLs.Add (url);
					updated = true;
				}
			}
			if (updated && ImageURLsUpdatedEvent != null) {
				ImageURLsUpdatedEvent (m_ImageURLs);
			}
		}

		public void ClearImageURLS()
		{
			m_ImageURLs.Clear ();
		}

		public void ArchiveJob()
		{
			var url = m_UrlBase + m_ScriptName + "?tgz=1";
			SendWebRequest (url);
			PollForZip ();
		}

		private Timer m_ArchivePollingTimer;
		private WebClient m_CurrentArchivePollingClient;
		private string m_ArchiveJobName;
		public event Action<string> ArchiveDownloadCompletedEvent;
		public event Action<string, string> ArchiveDownloadErrorEvent;
		public event Action<DownloadProgressChangedEventArgs> ArchiveDownloadProgressChangedEvent;
		public string ArchiveDirectory { get; private set; }

		private void PollForZip()
		{
			var jobName = m_JDC.CurrentJob.Name;
			if(null == m_ArchivePollingTimer) {
				m_ArchivePollingTimer = new Timer (m_PollTime);
				m_ArchivePollingTimer.Elapsed += new ElapsedEventHandler (OnArchivePollingEvent);
			}
			m_ArchiveJobName = m_JDC.CurrentJob.Name;
			m_ArchivePollingTimer.Enabled = true;
			OnArchivePollingEvent (null, null);
		}

		private void OnArchivePollingEvent(object source, ElapsedEventArgs e)
		{
			SendArchivePollingWebRequest (Uri.EscapeUriString (m_UrlBase + m_JobsFolder + m_ArchiveJobName + ".tgz"));
		}

		private void SendArchivePollingWebRequest(string url)
		{
			if(m_CurrentArchivePollingClient != null) {
				// If it's downloading, we don't want to cancel and start a new one.
				if (m_CurrentArchivePollingClient.IsBusy)
					return;
				m_CurrentArchivePollingClient.CancelAsync ();
				m_CurrentArchivePollingClient = null;
			}

			m_CurrentArchivePollingClient = new WebClient ();
			m_CurrentArchivePollingClient.DownloadFileCompleted += (sender, e) => {
				if(e.Cancelled) {
					if(ArchiveDownloadErrorEvent != null) {
						ArchiveDownloadErrorEvent(m_ArchiveJobName, "Cancelled");
					}
					return;
				}
					
				if(e.Error != null) {
					if(ArchiveDownloadErrorEvent != null) {
						ArchiveDownloadErrorEvent(m_ArchiveJobName, e.Error.Message);
					}
					return;
				}

				if(ArchiveDownloadCompletedEvent != null) {
					ArchiveDownloadCompletedEvent(m_ArchiveJobName);
				}
				m_ArchivePollingTimer.Enabled = false;
			};
			m_CurrentArchivePollingClient.DownloadProgressChanged += DownloadArchiveProgressChanged;
			m_CurrentArchivePollingClient.DownloadFileAsync (new Uri (url), Path.Combine(ArchiveDirectory, m_ArchiveJobName+".tgz"));
		}

		private void DownloadArchiveProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		{
			if (ArchiveDownloadProgressChangedEvent != null)
				ArchiveDownloadProgressChangedEvent (e);
		}
	}
}

