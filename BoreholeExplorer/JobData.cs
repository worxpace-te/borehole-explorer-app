﻿using System;
using System.Collections.Generic;

namespace BoreholeExplorer
{
	public class JobData : Java.Lang.Object
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTime CreationDateTime { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public SiteDataClass SiteData { get; set; }

		public List<HoleData> m_HoleDataList;
		public bool HasSiteImage { get; set; }
		public bool IsArchived { get; set; }
		public bool IsUploaded { get; set; }

		public bool IsCompleted { 
			get { 
				if (m_HoleDataList == null || m_HoleDataList.Count == 0) {
					// cant be compelte if there are no holes
					return false;
				}
				foreach (var hole in m_HoleDataList) {
					if (!hole.HasSiteImage || !hole.HasHoleImages) {
						// if one hole is not completed, then the job isn't either
						return false;
					}
				}
				return HasSiteImage;
			}
		}

		public JobData ()
		{
			m_HoleDataList = new List<HoleData> ();
			SiteData = new SiteDataClass ();
		}

		public JobData( string name, string description, List<HoleData> holeDataList )
		{
			Name = name;
			Description = description;
			SiteData = new SiteDataClass ();
			m_HoleDataList = holeDataList;
		}
	}
}

