﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace BoreholeExplorer
{
	public class HoleListAdapter : BaseAdapter
	{
		private List<HoleData> m_HoleDataList;
		LayoutInflater m_Inflater;
		Context m_Context;

		public HoleListAdapter(Context context, List<HoleData> holeData)
		{
			m_HoleDataList = holeData;
			m_Context = context;
			m_Inflater = LayoutInflater.From (m_Context);
		}

		public override int Count {
			get {
				if (m_HoleDataList == null)
					return 0;
				return m_HoleDataList.Count;
			}
		}

		public override Java.Lang.Object GetItem (int position)
		{
			return (Java.Lang.Object)m_HoleDataList[position];
		}

		public override long GetItemId (int position)
		{
			return 0;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			HoleListElement hle;

			if (convertView == null) {
				convertView = m_Inflater.Inflate (Resource.Layout.HoleListElement, null);
				hle = new HoleListElement ();
				convertView.Tag = hle;
			} else {
				hle = (HoleListElement)convertView.Tag;
			}

			hle.nameView = detail (convertView, Resource.Id.holeNameTextView, m_HoleDataList [position].Name);
			hle.holeSiteImageCapturedImageView = detail(convertView, Resource.Id.holeSiteImageCapturedImageView, m_HoleDataList[position].HasSiteImage ? Resource.Drawable.joblist_ok : Resource.Drawable.joblist_pending);
			hle.holeHoleImagesCapturedImageView = detail(convertView, Resource.Id.holeHoleImagesCapturedImageView, m_HoleDataList[position].HasHoleImages ? Resource.Drawable.joblist_ok : Resource.Drawable.joblist_pending);

			return convertView;
		}

		private TextView detail(View v, int resId, string text) 
		{
			TextView tv = (TextView)v.FindViewById (resId);
			tv.Text = text;
			return tv;
		}

		private ImageView detail(View v, int resId, int icon) 
		{
			ImageView iv = (ImageView)v.FindViewById (resId);
			iv.SetImageResource (icon);
			return iv;
		}

		private class HoleListElement : Java.Lang.Object
		{
			public TextView nameView;
			public ImageView holeSiteImageCapturedImageView;
			public ImageView holeHoleImagesCapturedImageView;
		}
	}
}

