﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace BoreholeExplorer
{
	public class JobListAdapter : BaseAdapter
	{
		private List<JobData> m_List;
		LayoutInflater m_Inflater;
		Context m_Context;

		public JobListAdapter(Context context, List<JobData> list)
		{
			m_List = list;
			m_Context = context;
			m_Inflater = LayoutInflater.From (m_Context);
		}

		public override int Count {
			get {
				return ( m_List == null ) ? 0 : m_List.Count;
			}
		}

		public override Java.Lang.Object GetItem (int position)
		{
			return (Java.Lang.Object)m_List[position];
		}

		public override long GetItemId (int position)
		{
			return 0;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			JobListElement jobListElement;

			if (convertView == null) {
				convertView = m_Inflater.Inflate (Resource.Layout.JobListElement, null);
				jobListElement = new JobListElement ();
				convertView.Tag = jobListElement;
			} else {
				jobListElement = (JobListElement)convertView.Tag;
			}

			jobListElement.jobTitle = detail (convertView, Resource.Id.jobNameTextView, m_List [position].Name);
			jobListElement.completedImageView = detail (convertView, Resource.Id.completedImageView, m_List [position].IsCompleted ? Resource.Drawable.joblist_ok : Resource.Drawable.joblist_pending);
			jobListElement.archivedImageView = detail (convertView, Resource.Id.archivedImageView, m_List [position].IsArchived ? Resource.Drawable.joblist_ok : Resource.Drawable.joblist_pending);
			jobListElement.uploadedImageView = detail (convertView, Resource.Id.uploadedImageView, m_List [position].IsUploaded ? Resource.Drawable.joblist_ok : Resource.Drawable.joblist_pending);

			return convertView;
		}

		private TextView detail(View v, int resId, string text) {
			TextView tv = (TextView)v.FindViewById (resId);
			tv.Text = text;
			return tv;
		}

		private ImageView detail(View v, int resId, int icon) {
			ImageView iv = (ImageView)v.FindViewById (resId);
			iv.SetImageResource (icon);
			return iv;
		}

		private class JobListElement : Java.Lang.Object
		{
			public TextView jobTitle;
			public ImageView completedImageView;
			public ImageView archivedImageView;
			public ImageView uploadedImageView;
		}
	}
}

