﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using UrlImageViewHelper;
using Android.Graphics;

namespace BoreholeExplorer
{
	public class HoleImageListAdapter : BaseAdapter
	{
		private List<HoleDataImage> m_HoleDataImages;
		LayoutInflater m_Inflater;
		Context m_Context;
		private HoleViewActivity m_HoleViewActivity;

		public HoleImageListAdapter(Context context, List<HoleDataImage> holeDataImages)
		{
			m_HoleDataImages = holeDataImages;
			m_Context = context;
			m_Inflater = LayoutInflater.From (m_Context);
		}

		public void SetHoleViewActivity(HoleViewActivity activity)
		{
			m_HoleViewActivity = activity;
		}

		public override Java.Lang.Object GetItem (int position)
		{
			return (Java.Lang.Object)m_HoleDataImages [position];
		}

		public override long GetItemId (int position)
		{
			return 0;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			HoleDataImageElement hdie;

			if (convertView == null) {
				convertView = m_Inflater.Inflate (Resource.Layout.HoleImageListElement, null);
				hdie = new HoleDataImageElement ();
				convertView.Tag = hdie;
			} else {
				hdie = (HoleDataImageElement)convertView.Tag;
				hdie.imageView.SetImageBitmap (null);
			}

			hdie.nameView = detail (convertView, Resource.Id.holeImageNameView, m_HoleDataImages [position].Name);

			hdie.imageView = detail (convertView, Resource.Id.holeImageView, Resource.Drawable.image_loading);

			Console.WriteLine ("Getting view for  : " + m_HoleDataImages [position].FilePath);

			if (!m_HoleDataImages [position].IsCaching) {
				m_HoleDataImages [position].CacheRemoteImage ((b) => {
					if (b) {
						//Console.WriteLine("Cached Remote image : " + m_HoleDataImages[position].FilePath);
						m_HoleViewActivity.LoadBitmapIntoImageView (m_HoleDataImages [position].FilePath, hdie.imageView);
					
					} else {
						Console.WriteLine ("Error caching image.");
					}
				});
			}

			return convertView;
		}

		private TextView detail(View v, int resId, string text) {
			TextView tv = (TextView)v.FindViewById (resId);
			tv.Text = text;
			return tv;
		}

		private SiteImageView detailURL(View v, int resId, string url) {
			SiteImageView iv = (SiteImageView)v.FindViewById (resId);
			iv.SetUrlDrawable (url);
			return iv;
		}

		private SiteImageView detail(View v, int resId, int drawableId)
		{
			SiteImageView iv = (SiteImageView)v.FindViewById (resId);
			iv.SetImageResource (drawableId);
			return iv;
		}

		public override int Count {
			get {
				return (m_HoleDataImages == null) ? 0 : m_HoleDataImages.Count;
			}
		}

		private class HoleDataImageElement : Java.Lang.Object
		{
			public TextView nameView;
			public SiteImageView imageView;
		}
	}
}

