﻿using System;
using System.Collections.Generic;

namespace BoreholeExplorer
{
	public class HoleData : Java.Lang.Object
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTime	CreationDateTime { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public List<HoleDataImage> m_HoleDataImages = new List<HoleDataImage> ();
		public bool HasSiteImage { get; set; }
		public bool HasHoleImages { get; set; }

		public HoleData ()
		{
		}
	}
}