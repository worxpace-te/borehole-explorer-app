using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Android.App;

namespace BoreholeExplorer
{
	public class JobDataController
	{
		private static JobDataController m_Instance;

		public static JobDataController Instance 
		{
			get {
				if (m_Instance == null) { 
					m_Instance = new JobDataController ();
				}
				return m_Instance;
			}
		}

		private List<JobData> m_JobDataList;
		private int m_CurrentJob = -1;
		private int m_CurrentHole = -1;

		public event Action CurrentJobChanged;
		public event Action CurrentHoleChanged;

		public JobData CurrentJob
		{
			get { 
				if (-1 == m_CurrentJob)	return null;
				return m_JobDataList [m_CurrentJob];
			}
		}

		public HoleData CurrentHole
		{
			get {
				if (-1 == m_CurrentHole) return null;
				var job = CurrentJob;
				if (null == job) return null;
				return job.m_HoleDataList [m_CurrentHole];
			}
		}

		private string m_JobsDataDirectory;
		private string m_JobSiteImagesDirectory;

		public JobDataController ()
		{
			var myDocumentsDirectory = Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments);
			m_JobsDataDirectory = Path.Combine (myDocumentsDirectory, "JobDataFiles");
			if (!Directory.Exists (m_JobsDataDirectory)) {
				Console.WriteLine ("Create Directory: " + m_JobsDataDirectory);
				Directory.CreateDirectory (m_JobsDataDirectory);
			}

			// Create the JobSiteImage directory if it does not already exist
			m_JobSiteImagesDirectory = System.IO.Path.Combine (myDocumentsDirectory, "JobSiteImages" );
			if (!System.IO.Directory.Exists (m_JobSiteImagesDirectory)) {
				System.Console.WriteLine ("Create Directory: " + m_JobSiteImagesDirectory);
				System.IO.Directory.CreateDirectory (m_JobSiteImagesDirectory);
			}
		}

		public void SetCurrentJob(int index)
		{
			if (index >= 0 && index < m_JobDataList.Count) {
				m_CurrentJob = index;
				m_CurrentHole = -1;
			} else {
				m_CurrentJob = -1;
				m_CurrentHole = -1;
			}
			if (null != CurrentHoleChanged)
				CurrentHoleChanged ();
			if (null != CurrentJobChanged)
				CurrentJobChanged ();

		}

		public void SetCurrentHole(int index)
		{
			var job = CurrentJob;
			if (null == job) {
				Console.WriteLine ("Error : Job not set yet");
				return;
			}
			if (index >= 0 && index < job.m_HoleDataList.Count) {
				m_CurrentHole = index;
			} else {
				Console.WriteLine ("Error : Hole index is out of range.  Setting to none : " + index);
				m_CurrentHole = -1;
			}
			if (null != CurrentHoleChanged)
				CurrentHoleChanged ();
		}

		public JobData GetJobAtIndex(int index)
		{
			if (m_JobDataList == null)
				return null;
			if (index >= 0 && index < m_JobDataList.Count) {
				return m_JobDataList [index];
			}
			return null;
		}

		public void AddJob( JobData job ) {
			m_JobDataList.Add (job);
			SaveJobDataToFiles ();
		}

		public void AddHole( JobData job, HoleData hole ) {
			job.m_HoleDataList.Add (hole);
			SaveJobDataToFiles ();
		}

		public void DeleteHole( JobData job, HoleData hole ) {
			job.m_HoleDataList.Remove (hole);
			SaveJobDataToFiles ();
		}

		public void DeleteJob( JobData job ) {
			m_JobDataList.Remove (job);
			SaveJobDataToFiles ();
		}

		public bool JobNameExists( string name ) {
			if (JobDataController.Instance.m_JobDataList == null)
				return false;
			foreach (var job in JobDataController.Instance.m_JobDataList) {
				if (name == job.Name) {
					return true;
				}
			}
			return false;
		}

		public bool HoleNameExists( JobData job, string name ) {
			if (job.m_HoleDataList == null)
				return false;
			foreach (var hole in job.m_HoleDataList) {
				if (name == hole.Name) {
					return true;
				}
			}
			return false;
		}

		public string GetSiteImagesDirectory() {
			return m_JobSiteImagesDirectory;
		}

		public string GetSiteDataDirectory() {
			return m_JobsDataDirectory;
		}

		public string GetSiteImageFilename( int jobIndex, int holeIndex ) {
			JobData job = JobDataController.Instance.GetJobAtIndex (jobIndex);
			if (holeIndex < 0) {
				// we are capturing a job site image
				return String.Format ("Job_" + job.Name + "_SiteImage.jpg");
			} else {
				// we are capturing a hole site image
				return String.Format ("Job_" + job.Name + "_Hole_" + job.m_HoleDataList [holeIndex].Name + "_SiteImage.jpg");
			}
		}

		public List<JobData> GetJobDataList() {

			if (m_JobDataList == null) {

				m_JobDataList = new List<JobData>();

				// Load job data from the file system
				try {
					var directoryFiles = Directory.EnumerateFiles(m_JobsDataDirectory, "*.xml", SearchOption.TopDirectoryOnly);
					foreach (var file in directoryFiles ) {
						Console.WriteLine ("File Read JobData from:" + file);
						string jobDataString = File.ReadAllText (file);
						JobData jobDataObject = Serializer.Deserialize<JobData> (jobDataString);
						m_JobDataList.Add (jobDataObject);
					}
				} catch (Exception ex) {
					Console.Error.WriteLine ("GetJobDataList", "Couldn't access the directory " + m_JobsDataDirectory + "; " + ex);
					return null;
				}
			}

			return m_JobDataList;

		}

		public void SaveJobDataToFiles() {
			string jobDataString;
			string jobFilenameString;

			// erase all existing job files if there are any
			Console.WriteLine ("SaveJobDataToFiles - erase all exsiting jobs from disk before saving new JobData");
			try {
				System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(m_JobsDataDirectory);
				foreach (FileInfo file in downloadedMessageInfo.GetFiles())
				{
					file.Delete(); 
				}
			} catch (Exception ex) {
				Console.Error.WriteLine ("SaveJobDataToFiles", "Couldn't access the directory " + m_JobsDataDirectory + "; " + ex);
			}

			// Save jobdata
			Console.WriteLine ("SaveJobDataToFiles - save all Jobs");
			foreach (var job in m_JobDataList) 
			{
				jobDataString = Serializer.Serialize<JobData> (job);
				jobFilenameString = Path.Combine(m_JobsDataDirectory, "JobDataFile_" + job.Name + ".xml" );
				try {
					Console.WriteLine ("File Write JobData to:" + jobFilenameString);
					File.WriteAllText (jobFilenameString, jobDataString);
				} catch (Exception ex) {
					Console.Error.WriteLine ("SaveJobDatatoFiles", "WriteAllTextError:" + jobFilenameString + "; " + ex);
				}
			}
		}

		public HoleDataImage AddHoleImageURL(Activity activity, string name, string url)
		{
			if (-1 == m_CurrentJob) {
				Console.WriteLine ("No current job set. Ignoring Image URL : " + url);
				return null;
			}
			if (-1 == m_CurrentHole) {
				Console.WriteLine ("No current Hole set. Ignoring Image URL : " + url);
				return null;
			}

			var hole = CurrentHole;
			// Check if already in the list
			var found = hole.m_HoleDataImages.Any (item => item.RemoteURL == url);
			if (found) {
				// Already in list
				Console.WriteLine ("Already had image : " + url);
				return null;
			}

			var holeDataImage = new HoleDataImage (name, url);
			activity.RunOnUiThread (() => {
				CurrentHole.m_HoleDataImages.Add (holeDataImage);
			});
			return holeDataImage;
		}
	}
}

