﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Locations;
using System.Timers;
using Android.Gms.Common;
using Android.Gms.Location;

namespace BoreholeExplorer
{
	public class LocationController : Java.Lang.Object, Android.Gms.Location.ILocationListener, IGooglePlayServicesClientConnectionCallbacks, IGooglePlayServicesClientOnConnectionFailedListener 
	{
		private static LocationController m_Instance;

		public static LocationController Instance 
		{
			get {
				if (m_Instance == null) { 
					m_Instance = new LocationController ();
				}
				return m_Instance;
			}
		}

		//private Location m_CurrentLocation;
		private LocationClient m_LocationClient;

		public void Setup ( Context applicationContext )
		{
			/*
			LocationManager service = (LocationManager)GetSystemService(Location LOCATION_SERVICE);
			bool enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);

			// check if enabled and if not send user to the GSP settings
			// Better solution would be to display a dialog and suggesting to 
			// go to the settings
			if (!enabled) {
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);
			} 
			*/
			m_LocationClient = new LocationClient (applicationContext, this, this);
			m_LocationClient.Connect ();
		}

		public double GetLocationLatitude() {
			Console.WriteLine("GetLat:" + m_LocationClient.LastLocation.Latitude );
			return m_LocationClient.LastLocation.Latitude;
		}

		public double GetLocationLongitude() {
			Console.WriteLine("GetLong:" + m_LocationClient.LastLocation.Longitude );
			return m_LocationClient.LastLocation.Longitude;
		}

		public void OnLocationChanged (Location location)
		{
			Console.WriteLine ("LocationController OnLocationChanged");
		}

		public void OnConnected (Bundle p0)
		{
			Console.WriteLine ("LocationController OnConnected");
		}

		public void OnDisconnected ()
		{
			Console.WriteLine ("LocationController OnDisconnected");
		}

		public void OnConnectionFailed (ConnectionResult p0)
		{
			Console.WriteLine ("LocationController OnConnectionFailed");
		}

	}
}

