﻿using System;
using System.Net;
using System.IO;
using System.Text;

namespace BoreholeExplorer
{
	public class HoleDataImage : Java.Lang.Object
	{
		public string Name { get; set; }
		public string RemoteURL { get; set; }
		public string FilePath { get; set; }
		public bool IsCaching { get; set; }

		public HoleDataImage()
		{
		}

		public HoleDataImage(string name, string remoteURL)
		{
			RemoteURL = remoteURL;
			FilePath = ConvertRemoteURLToFilePath (remoteURL);
			Name = name;
		}

		public void CacheRemoteImage(Action<bool> cacheCompleted)
		{
			if (string.IsNullOrEmpty (RemoteURL)) {
				Console.WriteLine ("Error: No Remote URL set");
				return;
			}

			if ( String.IsNullOrEmpty(FilePath) ) {
				return;
			}

			if (File.Exists (FilePath)) {
				// we have already cached it, so call the callback
				if(cacheCompleted != null)
					cacheCompleted (true);
				return;
			}

			IsCaching = true;
			var webClient = new WebClient ();
			webClient.DownloadDataCompleted += (s, e) => {
				IsCaching = false;
				var bytes = e.Result; // get the downloaded data
				if(bytes == null){
					cacheCompleted(false);
					return;
				}
				File.WriteAllBytes(FilePath, bytes);
				Console.WriteLine("Downloaded file : " + RemoteURL);
				if(cacheCompleted != null)
					cacheCompleted(true);
			};

			var url = new Uri (RemoteURL);
			webClient.DownloadDataAsync (url);
		}

		private static string ConvertRemoteURLToFilePath(string remoteURL)
		{
			int index = remoteURL.IndexOf ("/jobs/");
			if (index == -1) {
				Console.WriteLine ("Error : /jobs wan't found in remoteURL : " + remoteURL);
				return "";
			}

			// Index + 6 to remove the /jobs/ componenet as well.
			var substring = remoteURL.Substring (index + 6);
			// Create the folders if they don't exist
			var splits = substring.Split ('/');
			var path = JobDataController.Instance.GetSiteImagesDirectory ();
			for (int i = 0; i < splits.Length - 1; i++) {
				path = Path.Combine (path, splits [i]);
				if (!Directory.Exists(path)) {
					Directory.CreateDirectory (path);
					Console.WriteLine ("Created directory : " + path);
				}
			}
			var fileURL = Path.Combine (JobDataController.Instance.GetSiteImagesDirectory (), substring);
			return fileURL;
		}
	}
}

