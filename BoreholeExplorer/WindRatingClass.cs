﻿using System;
using System.Collections.Generic;

namespace BoreholeExplorer
{
	public class WindRatingClass
	{
		public List<string> Region { get; set; }
		public List<string> Terrain { get; set; }
		public List<string> Shielding { get; set; }
		public List<string> Topographic { get; set; }
		public List<string> Rating { get; set; }

		public WindRatingClass ()
		{
			Region = new List<string> ();
			Terrain = new List<string> ();
			Shielding = new List<string> ();
			Topographic = new List<string> ();
			Rating = new List<string> ();
		}
	}
}

