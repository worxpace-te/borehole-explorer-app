﻿using System;
using Android.App;
using Android.Net.Wifi;

namespace BoreholeExplorer
{
	public static class WiFiCheck
	{
		public const string SSIDCheckString = "borehole";
		public static bool CheckForBoreholeSSID() {
			WifiManager wifiManager = (WifiManager)Application.Context.GetSystemService (Application.WifiService);
			WifiInfo wifiInfo = wifiManager.ConnectionInfo;
			bool SSIDOk = false;
			if ( wifiInfo.SSID.Length > 2 ) {
				SSIDOk = String.Compare (wifiInfo.SSID.Substring(1,wifiInfo.SSID.Length-2), SSIDCheckString) == 0;
				Console.WriteLine ("wifiInfo" + wifiInfo);
				Console.WriteLine ("SSID:" + wifiInfo.SSID + " Check " + SSIDCheckString + ":" + SSIDOk );
			}
			return SSIDOk;
		}

	}
}

