﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace BoreholeExplorer
{
	public class SiteImageView : ImageView
	{
		public string m_CurrentBitMap;
		public int m_TargetWidth;
		public int m_TargetHeight;

		public event Action OnSizeChangedEvent;

		public SiteImageView (Context context) :
			base (context)
		{
			Initialize ();
		}

		public SiteImageView (Context context, IAttributeSet attrs) :
			base (context, attrs)
		{
			Initialize ();
		}

		public SiteImageView (Context context, IAttributeSet attrs, int defStyle) :
			base (context, attrs, defStyle)
		{
			Initialize ();
		}

		void Initialize ()
		{
			m_CurrentBitMap = null;
		}

		protected override void OnSizeChanged (int w, int h, int oldw, int oldh)
		{
			base.OnSizeChanged (w, h, oldw, oldh);
			if (w != 0 && h != 0) {
				if (m_TargetWidth == 0) {
					m_TargetWidth = w;
					m_TargetHeight = h;
				}
				Console.WriteLine ("SiteImageView Size change w:" + w + " h:" + h);
				if ( OnSizeChangedEvent != null ) {
					OnSizeChangedEvent();
				}
			}
		}
	}
}

