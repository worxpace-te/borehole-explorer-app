﻿using System;
using System.Net;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace BoreholeExplorer
{
	public class FTPUploader : IProgress<int>
	{
		#region IProgress implementation

		public void Report (int value)
		{
			if (UploadProgressChangedEvent != null)
				UploadProgressChangedEvent (value);
		}

		#endregion

		#region Singleton
		private static FTPUploader m_Instance;

		public static FTPUploader Instance 
		{
			get {
				if (null == m_Instance) {
					m_Instance = new FTPUploader ();
				}
				return m_Instance;
			}
		}
		#endregion

		private string m_ServerLocation = "ftp://stpeters.worxpace.com/ftp/";
		private string m_UserName = "hartgeo";
		private string m_Password = "hg2044";

		private readonly string m_LockFileLocation;

		public event Action<bool> UploadStageCompletedEvent;
		public event Action<int> UploadProgressChangedEvent;
		public event Action UploadCompletedEvent;

		private NetworkCredential m_Credentials;

		private FTPUploader() 
		{
			m_Credentials = new NetworkCredential (m_UserName, m_Password);
			m_LockFileLocation = Path.Combine (EmbeddedController.Instance.ArchiveDirectory, "lock.txt");

			if(!File.Exists(m_LockFileLocation))
			{
				File.WriteAllText (m_LockFileLocation, "lock");
			}
		}

		public int UploadJobNew()
		{
			// Get count
			var imageFiles = Directory.GetFiles (JobDataController.Instance.GetSiteImagesDirectory (), "*" + JobDataController.Instance.CurrentJob.Name + "*.jpg");
			var jobFiles = Directory.GetFiles (JobDataController.Instance.GetSiteDataDirectory (), "*" + JobDataController.Instance.CurrentJob.Name + ".xml");
			// 4 == create dir, lock, tgz and unlock
			int count = 4 + imageFiles.Length + jobFiles.Length;
			RunUploadSequence ();
			return count;
		}

		private async void RunUploadSequence()
		{
			Console.WriteLine ("Upload Seq : 1");
			if (await CreateDirectory ()) {
				Console.WriteLine ("Upload Seq : Error");
				if (UploadCompletedEvent != null)
					UploadCompletedEvent ();
				return;
			}
			Console.WriteLine ("Upload Seq : 2");
			if (await UploadLock ()) {
				Console.WriteLine ("Upload Seq : Error");
				if (UploadCompletedEvent != null)
					UploadCompletedEvent ();
				return;
			}
			Console.WriteLine ("Upload Seq : 3");
			if (await UploadArchive ()) {
				Console.WriteLine ("Upload Seq : Error");
				if (UploadCompletedEvent != null)
					UploadCompletedEvent ();
				return;
			}
			Console.WriteLine ("Upload Seq : 4");
			var imageFiles = Directory.GetFiles (JobDataController.Instance.GetSiteImagesDirectory (), "*" + JobDataController.Instance.CurrentJob.Name + "*.jpg");
			Console.WriteLine ("Upload Seq : 5");
			for(int i = 0; i < imageFiles.Length; i++)
			{
				Console.WriteLine ("Upload Seq : 6 : " + i);
				if (await UploadImages (i)) {
					Console.WriteLine ("Upload Seq : Error");
					if (UploadCompletedEvent != null)
						UploadCompletedEvent ();
					return;
				}
				Console.WriteLine ("Upload Seq : 7 : " + i);
			}
			var jobFiles = Directory.GetFiles (JobDataController.Instance.GetSiteDataDirectory (), "*" + JobDataController.Instance.CurrentJob.Name + ".xml");
			Console.WriteLine ("Upload Seq : 8");
			for(int i = 0; i < jobFiles.Length; i++)
			{
				Console.WriteLine ("Upload Seq : 9 : " + i);
				if (await UploadJobs (i)) {
					Console.WriteLine ("Upload Seq : Error");
					if (UploadCompletedEvent != null)
						UploadCompletedEvent ();
					return;
				}
				Console.WriteLine ("Upload Seq : 10 : " + i);
			}
			Console.WriteLine ("Upload Seq : 11");
			if (await RemoveLock ()) {
				Console.WriteLine ("Upload Seq : Error");
				if (UploadCompletedEvent != null)
					UploadCompletedEvent ();
				return;
			}
			Console.WriteLine ("Upload Seq : 12");
			if (UploadCompletedEvent != null)
				UploadCompletedEvent ();
		}

		private async Task<bool> CreateDirectory()
		{
			bool exceptionError = false;
			await Task.Factory.StartNew (() => {
				var serverDir = m_ServerLocation + JobDataController.Instance.CurrentJob.Name;
				FtpWebRequest dirRequest = (FtpWebRequest)FtpWebRequest.Create (serverDir);
				dirRequest.Method = WebRequestMethods.Ftp.MakeDirectory;
				dirRequest.UsePassive = false;
				dirRequest.UseBinary = true;
				dirRequest.Credentials = m_Credentials;
				FtpWebResponse dirResponse = null;
				try {
					dirResponse = (FtpWebResponse)dirRequest.GetResponse();
					Console.WriteLine ("FtpMakeDir Response Code : " + dirResponse.StatusCode);
				}
				catch(System.Exception e)
				{
					Console.WriteLine ("Exception : " + e.Message);
					exceptionError = true;
				}
				finally
				{
					if(dirResponse != null)
						dirResponse.Close();
					if (UploadStageCompletedEvent != null)
						UploadStageCompletedEvent (exceptionError);
				}
			});
			return exceptionError;
		}

		private async Task<bool> UploadLock()
		{
			bool exceptionError = false;
			await Task.Factory.StartNew (() => {
				exceptionError = UploadFile (m_LockFileLocation, m_ServerLocation + JobDataController.Instance.CurrentJob.Name + "/lock.txt", m_Credentials);
				if (UploadStageCompletedEvent != null)
					UploadStageCompletedEvent (exceptionError);
			});
			return exceptionError;
		}

		private async Task<bool> UploadArchive()
		{
			bool exceptionError = false;
			await Task.Factory.StartNew (() => {
				Console.WriteLine ("Uploading Archive...");
				exceptionError = UploadFile (Path.Combine (EmbeddedController.Instance.ArchiveDirectory, JobDataController.Instance.CurrentJob.Name + ".tgz"),
					m_ServerLocation + JobDataController.Instance.CurrentJob.Name + "/" + JobDataController.Instance.CurrentJob.Name + ".tgz", m_Credentials);
				Console.WriteLine("Uploading Archive Complete");
				if (UploadStageCompletedEvent != null)
					UploadStageCompletedEvent (exceptionError);
			});
			return exceptionError;
		}

		private async Task<bool> UploadImages(int index)
		{
			bool exceptionError = false;
			await Task.Factory.StartNew (() => {
				var imageFiles = Directory.GetFiles (JobDataController.Instance.GetSiteImagesDirectory (), "*" + JobDataController.Instance.CurrentJob.Name + "*.jpg");

				var split = imageFiles [index].Split ('/');
				Console.WriteLine("Uploading ImageFile : " + imageFiles[index] + " : to : " + m_ServerLocation + JobDataController.Instance.CurrentJob.Name + "/" + split [split.Length - 1]);
				exceptionError = UploadFile (imageFiles [index], m_ServerLocation + JobDataController.Instance.CurrentJob.Name + "/" + split [split.Length - 1], m_Credentials);
				if (UploadStageCompletedEvent != null)
					UploadStageCompletedEvent (exceptionError);
			});
			return exceptionError;
		}

		private async Task<bool> UploadJobs(int index)
		{
			bool exceptionError = false;
			await Task.Factory.StartNew (() => {
				var jobFiles = Directory.GetFiles (JobDataController.Instance.GetSiteDataDirectory (), "*" + JobDataController.Instance.CurrentJob.Name + ".xml");

				var split = jobFiles [index].Split ('/');
				Console.WriteLine ("Uploading JobFile : " + jobFiles [index] + " : to : " + m_ServerLocation + JobDataController.Instance.CurrentJob.Name + "/" + split [split.Length - 1]);
				exceptionError = UploadFile (jobFiles [index], m_ServerLocation + JobDataController.Instance.CurrentJob.Name + "/" + split [split.Length - 1], m_Credentials);
				if (UploadStageCompletedEvent != null)
					UploadStageCompletedEvent (exceptionError);
			});
			return exceptionError;
		}

		private async Task<bool> RemoveLock()
		{
			bool exceptionError = false;
			await Task.Factory.StartNew (() => {
				FtpWebRequest lockDestroyRequest = (FtpWebRequest)FtpWebRequest.Create (m_ServerLocation + JobDataController.Instance.CurrentJob.Name + "/lock.txt");
				lockDestroyRequest.Method = WebRequestMethods.Ftp.DeleteFile;
				lockDestroyRequest.Credentials = m_Credentials;
				lockDestroyRequest.UsePassive = false;
				FtpWebResponse lockDestroyResponse = null;
				try {
					lockDestroyResponse = (FtpWebResponse) lockDestroyRequest.GetResponse();
					Console.WriteLine ("Lock Destroy Response : " + lockDestroyResponse.StatusDescription);
				} catch (Exception e) {
					Console.WriteLine ("Exception uploading file : " + e.Message);
					exceptionError = true;
				} finally {
					lockDestroyResponse.Close ();
				}
			});
			return exceptionError;
		}

		private bool UploadFile(string file, string destination, ICredentials credentials)
		{
			bool exceptionError = false;
			FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create (destination);
			request.Method = WebRequestMethods.Ftp.UploadFile;
			request.UsePassive = false;
			request.Credentials = credentials;
			request.UseBinary = true;

			byte[] fileContents = File.ReadAllBytes (file);
			request.ContentLength = fileContents.Length;
			int uploadCount = 0;

			Console.WriteLine ("Uploading File : " + file + " : To : " + destination);
			Console.WriteLine ("File Size : " + fileContents.Length);
			Stream requestStream = null;
			try {
				Console.WriteLine ("1");
				
				requestStream = request.GetRequestStream ();
				Console.WriteLine ("2");
				while (uploadCount < fileContents.Length) {
					var uploadBytes = fileContents.Length - uploadCount >= 4096 ? 4096 : fileContents.Length - uploadCount;
					requestStream.Write (fileContents, uploadCount, uploadBytes);
					uploadCount += uploadBytes;
					Report ((int)((uploadCount / (float)fileContents.Length) * 100));
				}
			} catch (Exception e) {
				Console.WriteLine ("Exception uploading file : " + e.Message);
				exceptionError = true;
			} finally {
				if (requestStream != null)
					requestStream.Close ();
			}
			return exceptionError;
		}
	}
}

