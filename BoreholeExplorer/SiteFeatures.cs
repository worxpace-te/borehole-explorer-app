﻿using System;
using System.Collections.Generic;

namespace BoreholeExplorer
{
	public class SiteFeatures
	{
		public List<string> Grasses { get; set; }
		public List<string> Trees { get; set; }
		public List<string> Structures { get; set; }

		public SiteFeatures ()
		{
			Grasses = new List<string> ();
			Trees = new List<string> ();
			Structures = new List<string> ();
		}
	}
}

