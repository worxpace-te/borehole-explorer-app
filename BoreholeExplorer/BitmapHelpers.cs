using System.IO;

using Android.Graphics;
using System.Threading.Tasks;

namespace BoreholeExplorer
{

    public static class BitmapHelpers
    {
        public static Bitmap LoadAndResizeBitmap(this string fileName, int width, int height)
        {
			if (width == 0)	return null;
			if (height == 0) return null;

            // First we get the the dimensions of the file on disk
            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
            BitmapFactory.DecodeFile(fileName, options);

			System.Console.WriteLine ("Resizebitmap W:" + width + " h:" + height);
			System.Console.WriteLine ("BW:" + options.OutWidth + " BH:" + options.OutHeight);

            // Next we calculate the ratio that we need to resize the image by
            // in order to fit the requested dimensions.
            int outHeight = options.OutHeight;
            int outWidth = options.OutWidth;
			float inSampleSize = 1;

			/*if (outHeight > height || outWidth > width)
            {
                inSampleSize = outWidth > outHeight
					? (float)outHeight / (float)height
					: (float)outWidth / (float)width;
            }*/

			// in our case, we just want to make sure the height is correct
			if (outHeight > height)
			{
				inSampleSize = (float)outHeight / (float)height;
			}

            // Now we will load the image and have BitmapFactory resize it for us.
			options.InSampleSize = (int)System.Math.Ceiling(inSampleSize);
            options.InJustDecodeBounds = false;
            Bitmap resizedBitmap = BitmapFactory.DecodeFile(fileName, options);

			if (resizedBitmap != null) {
				System.Console.WriteLine ("Resizebitmap ISS:" + inSampleSize + "(" + options.InSampleSize + ") W:" + resizedBitmap.Width + " h:" + resizedBitmap.Height);
			} else {
				System.Console.WriteLine ("Resizebitmap null");
			}

            return resizedBitmap;
        }

		public static async Task<Bitmap> LoadAndResizeBitmapToWidthAsync(this string fileName, int width)
		{
			if (width == 0)	return null;

			// First we get the the dimensions of the file on disk
			BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
			Task<Bitmap> getBitmapSizeTask = BitmapFactory.DecodeFileAsync(fileName, options);

			using(var dispose = await getBitmapSizeTask) {}

			System.Console.WriteLine ("Resizebitmap W:" + width + " : " + fileName);
			System.Console.WriteLine ("BW:" + options.OutWidth + " BH:" + options.OutHeight);

			// Next we calculate the ratio that we need to resize the image by
			// in order to fit the requested dimensions.
			int outHeight = options.OutHeight;
			int outWidth = options.OutWidth;
			float ratio = (float)outWidth / (float)width;
			//float inSampleSize = 1;

			/*if (outHeight > height || outWidth > width)
            {
                inSampleSize = outWidth > outHeight
					? (float)outHeight / (float)height
					: (float)outWidth / (float)width;
            }*/

			// in our case, we just want to make sure the height is correct
			//if (outHeight > height)
			//{
			//	inSampleSize = (float)outHeight / (float)height;
			//}

			// Now we will load the image and have BitmapFactory resize it for us.
			//options.InSampleSize = (int)System.Math.Ceiling(inSampleSize);
			//opti
			options.InSampleSize = (int)System.Math.Ceiling (ratio);
			options.InJustDecodeBounds = false;

			Task<Bitmap> getBitmapTask = BitmapFactory.DecodeFileAsync(fileName, options);
			Bitmap resizedBitmap = await getBitmapTask;

			System.Console.WriteLine ("Resizebitmap ISS:" + ratio + "(" + options.InSampleSize + ") W:" + resizedBitmap.Width + " h:" + resizedBitmap.Height);

			return resizedBitmap;
		}
    }
}
